package com.chemproject.game.Utils;

import com.badlogic.gdx.graphics.Color;

public enum CharacterSkin {
    WHITE("White", 1.0f, 1.0f, 1.0f),
    GRAY("Gray", 0.7f, 0.7f, 0.7f),
    BROWN("Brown", 0.7f, 0.5f, 0.3f);
    
    private Color color;
    private String name;

    private CharacterSkin(String name, float r, float g, float b) {
        this.color = new Color();
        this.name = name;
        this.color.set(r, g, b, 1.0f);
    }

    public Color getColor() {
        return this.color;
    }

    public String toString() {
        return this.name;
    }
}
