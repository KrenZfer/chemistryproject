package com.chemproject.game.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.chemproject.game.Screens.SreenTransition.ScreenTransitionFade;
import com.chemproject.game.Screens.SreenTransition.ScreenTransitionSlice;
import com.chemproject.game.Screens.SreenTransition.ScreenTransitionSlide;

public class Constants {
    public static final int BIG_SCREEN_HEIGHT = 1080;
    public static final int BIG_SCREEN_WIDTH = 1920;
    public static final String DATA_CONVERSATION = "Data/ConversationData.json";
    public static final String DATA_KARAKTER = "Data/KoleksiKarakterData.json";
    public static final String DATA_SOLUTION_JSON = "Data/PasanganPenyangga.json";
    public static final String DATA_PANDUAN_JSON = "Data/TeksPanduan.json";
    public static final String LEVEL_01 = "levels/level-01.png";
    public static final String LEVEL_CHEM = "levels/levelChem.png";
    public static final int LIVES_START = 3;
    public static final String PREFERENCES = "GameCanyonBunnyPreferences";
    public static final int SCREEN_HEIGHT = 720;
    public static final int SCREEN_WIDTH = 1280;
    public static final String SKIN_GAME_UI = "images-ui/bunny_ui.json";
    public static final String SKIN_LIBGDX_UI = "images-ui/uiskin.json";
    public static final String SKIN_MENU_IN_GAME = "images-ui/menuingame.json";
    public static final String TAG = CameraHelper.class.getName();
    public static final String TEXTURE_ATLAS_BOOK = "images/choose_book.atlas";
    public static final String TEXTURE_ATLAS_BOOK_CONTENT = "images/konten_buku.atlas";
    public static final String TEXTURE_ATLAS_FORMULA_CONTENT = "images/larutan.atlas";
    public static final String TEXTURE_ATLAS_KARAKTER = "images/karakter.atlas";
    public static final String TEXTURE_ATLAS_LIBGDX_UI = "images-ui/uiskin.atlas";
    public static final String TEXTURE_ATLAS_OBJECTS = "images/bunny_pack.atlas";
    public static final String TEXTURE_ATLAS_SOAL = "images/soallevel3.atlas";
    public static final String TEXTURE_ATLAS_SOAL_LATIHAN = "images/soalLatihan.atlas";
    public static final String TEXTURE_ATLAS_UI = "images-ui/bunny_ui.atlas";
    public static final String TEXTURE_ATLAS_MENU_IN_GAME = "images-ui/menuingame.atlas";
    public static final String TEXT_SUMBER = "Untuk lebih jelasnya kalian bisa membuka";
    public static final float TIME_DELAY_GAME_OVER = 3.0f;
    public static final String URI_1 = "@Sumber 1=https://www.zenius.net/c/5141/larutan-buffer@";
    public static final String URI_2 = "@Sumber 2=https://www.bisakimia.com/2012/11/21/buffer-larutan-penyangga/@";
    public static final float VIEWPORT_GUI_HEIGHT = ((float) Gdx.app.getGraphics().getHeight());
    public static final float VIEWPORT_GUI_WIDTH = ((float) Gdx.app.getGraphics().getWidth());
    public static final float VIEWPORT_HEIGHT = 8.0f;
    public static final float VIEWPORT_WIDTH = 8.0f;
    public static float finalScoreLatihan = 0.0f;
    public static float finalScoreLevel1 = 0.0f;
    public static float finalScoreLevel2 = 0.0f;
    public static float finalScoreLevel3 = 0.0f;
    public static final ScreenTransitionFade screenFade = ScreenTransitionFade.init(0.75f);
    public static final ScreenTransitionSlice screenSlice = ScreenTransitionSlice.init(0.75f, 2, 5, Interpolation.elastic);
    public static final ScreenTransitionSlide screenSlide = ScreenTransitionSlide.init(0.75f, 4, false, Interpolation.elastic);

    public class PreferenceConstants {
        public static final String AUDIO_SETTINGS_NAME = "Audio";
        public static final String CANCEL_BUTTON_NAME = "Cancel";
        public static final String CHARSKIN_SETTINGS_NAME = "Character Skin";
        public static final String CHAR_SKIN_PREFERENCES = "char_skin";
        public static final String MUSIC_PREFERENCES = "music_pref";
        public static final String MUSIC_SETTINGS_NAME = "Music";
        public static final String OPTION_WINDOW_NAME = "Option";
        public static final String SAVE_BUTTON_NAME = "Save";
        public static final String SHOW_FPS_COUNTER_PREFERENCES = "show_fps_counter";
        public static final String SOUND_PREFERENCES = "sound_pref";
        public static final String SOUND_SETTINGS_NAME = "Sound";
        public static final String VOLUME_MUSIC_PREFERENCES = "vol_music";
        public static final String VOLUME_SOUND_PREFERENCES = "vol_sound";
    }

    public static float getRealWidth(float width) {
        return (width / 1280.0f) * VIEWPORT_GUI_WIDTH;
    }

    public static float getRealHeight(float height) {
        return (height / 720.0f) * VIEWPORT_GUI_HEIGHT;
    }
}
