package com.chemproject.game.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ScientificTextBuilder {
    public static final String TAG = ScientificTextBuilder.class.getName();

    public static String[] makeEtoPower(float value) {
        if (Float.isInfinite(value)) {
            Gdx.app.error(TAG, "Passed value is Infinite, value: " + value);
            return new String[]{"Inf.", "", " "};
        } else if (Float.isNaN(value)) {
            Gdx.app.error(TAG, "Passed value is Not a Number, value: " + value);
            return new String[]{"NaN", "", " "};
        } else {
            Matcher match = Pattern.compile("-?\\d+(\\.\\d+)?").matcher(String.valueOf(value));
            ArrayList<String> matches = new ArrayList();
            while (match.find()) {
                matches.add(match.group());
            }
            try {
                String multiplier = String.valueOf(((float) Math.round(100.0d * Double.valueOf((String) matches.get(0)).doubleValue())) / 100.0f) + " * ";
                String power = (String) matches.get(1);
                return new String[]{multiplier, "10", power};
            } catch (IndexOutOfBoundsException e) {
                Gdx.app.error(TAG, "Caught IndexOutOfBoundsException - RegEx can not parse provided value, reporting value as Infinite, value: " + value);
                return new String[]{String.valueOf(value), "", ""};
            }
        }
    }

    public static Group createSuperScriptText(String preText, String[] EtoPower, String postText, LabelStyle labelStyle) {
        Group group = new Group();
        Label preLabel = new Label((CharSequence) preText, labelStyle);
        group.addActor(preLabel);
        Label multiplierLabel = new Label(EtoPower[0], labelStyle);
        multiplierLabel.setPosition(preLabel.getX() + preLabel.getPrefWidth(), preLabel.getY());
        group.addActor(multiplierLabel);
        Label baseLabel = new Label(EtoPower[1], labelStyle);
        baseLabel.setPosition(multiplierLabel.getX() + multiplierLabel.getPrefWidth(), multiplierLabel.getY());
        group.addActor(baseLabel);
        float superscriptOFFSET = baseLabel.getPrefHeight() / 3.0f;
        Label powerLabel = new Label(EtoPower[2], labelStyle);
        powerLabel.setFontScale(0.8f);
        powerLabel.setPosition(baseLabel.getX() + baseLabel.getPrefWidth(), baseLabel.getY() + superscriptOFFSET);
        group.addActor(powerLabel);
        Label postLabel = new Label((CharSequence) postText, labelStyle);
        postLabel.setPosition(powerLabel.getX() + powerLabel.getPrefWidth(), baseLabel.getY());
        group.addActor(postLabel);
        group.setHeight(baseLabel.getPrefHeight() + superscriptOFFSET);
        group.setWidth((((preLabel.getPrefWidth() + multiplierLabel.getPrefWidth()) + baseLabel.getPrefWidth()) + powerLabel.getPrefWidth()) + postLabel.getPrefWidth());
        return group;
    }
}
