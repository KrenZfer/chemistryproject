package com.chemproject.game.Utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.chemproject.game.ChemistryProject;

public class TextRenderer {
    private static final float PADDING = 10.0f;

    enum RenderMode {
        NORMAL,
        SUBSCRIPT,
        SUPERSCRIPT,
        LINK
    }

    public static Table renderString(final ChemistryProject game, Skin skin, String string, Color color, int width, int height) {
        FlowTable table = new FlowTable(skin);
        if (width != -1) {
            table.setWidth((float) width);
        }
        if (height != -1) {
            table.setHeight((float) height);
        }
        table.left();
        StringBuilder input = new StringBuilder(string);
        StringBuilder currentBlock = new StringBuilder();
        RenderMode currentMode = RenderMode.NORMAL;
        while (input.length() >= 1) {
            char currentChar = input.charAt(0);
            input.deleteCharAt(0);
            boolean endOfBlock = false;
            RenderMode newMode = null;
            switch (currentMode) {
                case NORMAL:
                    endOfBlock = true;
                    if (currentChar != '_') {
                        if (currentChar != '^') {
                            if (currentChar != '@') {
                                currentBlock.append(currentChar);
                                endOfBlock = false;
                                break;
                            }
                            newMode = RenderMode.LINK;
                            break;
                        }
                        newMode = RenderMode.SUPERSCRIPT;
                        break;
                    }
                    newMode = RenderMode.SUBSCRIPT;
                    break;
                case SUBSCRIPT:
                    if (currentChar != '_') {
                        currentBlock.append(currentChar);
                        break;
                    }
                    newMode = RenderMode.NORMAL;
                    endOfBlock = true;
                    break;
                case SUPERSCRIPT:
                    if (currentChar != '^') {
                        currentBlock.append(currentChar);
                        break;
                    }
                    newMode = RenderMode.NORMAL;
                    endOfBlock = true;
                    break;
                case LINK:
                    if (currentChar != '@') {
                        currentBlock.append(currentChar);
                        break;
                    }
                    newMode = RenderMode.NORMAL;
                    endOfBlock = true;
                    break;
            }
            if ((endOfBlock || input.length() == 0) && currentBlock.length() > 0) {
                LabelStyle style = new LabelStyle(Assets.instance.fonts.defaultNormal, color);
                switch (currentMode) {
                    case NORMAL:
                        table.add(new Label(currentBlock.toString(), style)).align(8);
                        break;
                    case SUBSCRIPT:
                        table.add(new Label(currentBlock.toString(), style)).padTop((float) PADDING);
                        break;
                    case SUPERSCRIPT:
                        table.add(new Label(currentBlock.toString(), style)).padBottom((float) PADDING);
                        break;
                    case LINK:
                        final String[] temp = currentBlock.toString().split("=");
                        TextButton tbt = new TextButton(temp[0], Assets.instance.skinLibGdx);
                        tbt.addListener(new ChangeListener() {
                            public void changed(ChangeEvent event, Actor actor) {
                                game.uriHandler.openURI(temp[1]);
                            }
                        });
                        table.add(tbt);
                        break;
                }
                currentBlock = new StringBuilder();
            }
            if (newMode != null) {
                currentMode = newMode;
            }
        }
        return table;
    }

    public static Table renderString(ChemistryProject game, Skin skin, String string, Color color) {
        return renderString(game, skin, string, color, -1, -1);
    }
}
