package com.chemproject.game.Utils;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class AudioManager {
    public static final AudioManager instance = new AudioManager();
    private Music playingMusic;

    private AudioManager() {
    }

    public void play(Sound sound) {
        play(sound, 1.0f);
    }

    public void play(Sound sound, float volume) {
        play(sound, volume, 1.0f);
    }

    public void play(Sound sound, float volume, float pitch) {
        play(sound, volume, pitch, 0.0f);
    }

    public void play(Sound sound, float volume, float pitch, float pan) {
        if (GamePreferences.instance.sound) {
            sound.play(GamePreferences.instance.volSound * volume, pitch, pan);
        }
    }

    public void play(Music music) {
        stopMusic();
        this.playingMusic = music;
        if (GamePreferences.instance.music) {
            music.setLooping(true);
            music.setVolume(GamePreferences.instance.volMusic);
            music.play();
        }
    }

    public void stopMusic() {
        if (this.playingMusic != null) {
            this.playingMusic.stop();
        }
    }

    public void onSettingUpdated() {
        if (this.playingMusic != null) {
            this.playingMusic.setVolume(GamePreferences.instance.volMusic);
            if (!GamePreferences.instance.music) {
                this.playingMusic.pause();
            } else if (!this.playingMusic.isPlaying()) {
                this.playingMusic.play();
            }
        }
    }
}
