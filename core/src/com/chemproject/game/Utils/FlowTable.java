package com.chemproject.game.Utils;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

public class FlowTable extends Table {
    public FlowTable(Skin skin) {
        super(skin);
    }

    public <T extends Actor> Cell<T> add(T actor) {
        float oldWith = getWidth();
        Cell<T> cell = super.add(actor);
        while (getWidth() > oldWith) {
            removeActor(actor);
            row();
            cell = super.add(actor);
        }
        return cell;
    }
}
