package com.chemproject.game.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Json;
import com.chemproject.game.Model.ConversationData;
import com.chemproject.game.Model.KoleksiKarakter;
import com.chemproject.game.Model.KoleksiKarakter.Karakter;
import com.chemproject.game.Model.TeksPanduan;

import java.util.Iterator;

public class Assets implements Disposable, AssetErrorListener {
    public static final String TAG = Assets.class.getName();
    public static final Assets instance = new Assets();
    public AssetBook assetBook;
    public AssetBookContent assetBookContent;
    public AssetFormulaContent assetFormulaContent;
    private AssetManager assetManager;
    public ConversationPanelBuilder builder;
    public AssetBunny bunny;
    public AssetConversation dataConversation;
    public AssetFeather feather;
    public AssetFonts fonts;
    public AssetGoldCoin goldCoin;
    public AssetCharacter karakter;
    public AssetLevelDecoration levelDecoration;
    public AssetMusic music;
    public AssetPad pads;
    public AssetRock rock;
    public Skin skinLibGdx;
    public AssetSoalLatihan soalLatihan;
    public AssetSoal soalLevel3;
    public AssetSounds sounds;
    public AssetMenuInGame mig;
    public AssetPanduan panduan;

    public class AssetBook {
        public final AtlasRegion background;
        public final AtlasRegion bgDialog;
        public final AtlasRegion bioBook;
        public final AtlasRegion bookShelf;
        public final AtlasRegion chemBook;
        public final AtlasRegion closeButton;
        public final AtlasRegion folkloreBook;
        public final AtlasRegion nextPage;
        public final AtlasRegion openBook;

        public AssetBook(TextureAtlas atlas) {
            this.background = atlas.findRegion("background");
            this.openBook = atlas.findRegion("openBook");
            this.bookShelf = atlas.findRegion("bookShelf");
            this.chemBook = atlas.findRegion("chemBook");
            this.bioBook = atlas.findRegion("bioBook");
            this.folkloreBook = atlas.findRegion("folkloreBook");
            this.closeButton = atlas.findRegion("closeButton");
            this.nextPage = atlas.findRegion("nextPage");
            this.bgDialog = atlas.findRegion("bgDialog");
        }
    }

    public class AssetBookContent {
        public final AtlasRegion level1;
        public final AtlasRegion level21;
        public final AtlasRegion level22;
        public final AtlasRegion level23;
        public final AtlasRegion level31;
        public final AtlasRegion level32;
        public final AtlasRegion level33;

        public AssetBookContent(TextureAtlas atlas) {
            this.level1 = atlas.findRegion("level1");
            this.level21 = atlas.findRegion("level21");
            this.level22 = atlas.findRegion("level22");
            this.level23 = atlas.findRegion("level23");
            this.level31 = atlas.findRegion("level31");
            this.level32 = atlas.findRegion("level32");
            this.level33 = atlas.findRegion("level33");
        }
    }

    public class AssetBunny {
        public final AtlasRegion head;
        public final AtlasRegion heart;

        public AssetBunny(TextureAtlas atlas) {
            this.head = atlas.findRegion("bunny_head");
            this.heart = atlas.findRegion("heart");
        }
    }

    public class AssetCharacter {
        public KoleksiKarakter koleksi = ((KoleksiKarakter) new Json().fromJson(KoleksiKarakter.class, Gdx.files.internal(Constants.DATA_KARAKTER)));
        public ArrayMap<String, AtlasRegion> koleksiKarakter = new ArrayMap();
        public final AtlasRegion fotoProfil;

        public AssetCharacter(TextureAtlas atlas) {
            fotoProfil = atlas.findRegion("fotoprofil");
            Iterator it = this.koleksi.getKoleksi().iterator();
            while (it.hasNext()) {
                Karakter i = (Karakter) it.next();
                this.koleksiKarakter.put(i.getPath(), atlas.findRegion(i.getPath()));
            }
        }
    }

    public class AssetConversation {
        public final ConversationData conversation = ((ConversationData) new Json().fromJson(ConversationData.class, Gdx.files.internal(Constants.DATA_CONVERSATION)));

        public AssetConversation() {
            this.conversation.setTotalScene(this.conversation.getDataJsonConversation().size);
        }
    }

    public class AssetFeather {
        public final AtlasRegion feather;

        public AssetFeather(TextureAtlas atlas) {
            this.feather = atlas.findRegion("item_feather");
        }
    }

    public class AssetFonts {
        public final BitmapFont defaultBig = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), false);
        public final BitmapFont defaultMedium = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), false);
        public final BitmapFont defaultNormal = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), false);
        public final BitmapFont defaultSmall = new BitmapFont(Gdx.files.internal("images/arial-15.fnt"), false);

        public AssetFonts() {
            this.defaultSmall.getData().setScale(Constants.getRealWidth(0.75f), Constants.getRealHeight(0.75f));
            this.defaultNormal.getData().setScale(Constants.getRealWidth(1.0f), Constants.getRealHeight(1.0f));
            this.defaultMedium.getData().setScale(Constants.getRealWidth(1.5f), Constants.getRealHeight(1.5f));
            this.defaultBig.getData().setScale(Constants.getRealWidth(2.0f), Constants.getRealHeight(2.0f));
            this.defaultSmall.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
            this.defaultNormal.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
            this.defaultMedium.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
            this.defaultBig.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        }
    }

    public class AssetFormulaContent {
        public final Array<AtlasRegion> larutanBukanPenyangga = new Array();
        public final Array<AtlasRegion> larutanPenyangga = new Array();
        public final NinePatch pieceFrame;
        public final AtlasRegion pieceFrameChoosen;
        public final AtlasRegion pieceFrameinActive;

        public AssetFormulaContent(TextureAtlas atlas) {
            this.pieceFrame = new NinePatch(atlas.findRegion("pieceFrame"), 12, 12, 12, 12);
            this.pieceFrameinActive = atlas.findRegion("pieceFrameinActive");
            this.pieceFrameChoosen = atlas.findRegion("pieceFrameChoosen");
            String penyangga = "penyangga1";
            String bukanPenyangga = "penyangga0";
            for (int i = 1; i <= 10; i++) {
                this.larutanPenyangga.add(atlas.findRegion(penyangga + String.valueOf(i)));
                this.larutanBukanPenyangga.add(atlas.findRegion(bukanPenyangga + String.valueOf(i)));
            }
        }
    }

    public class AssetGoldCoin {
        public final AtlasRegion coin;

        public AssetGoldCoin(TextureAtlas atlas) {
            this.coin = atlas.findRegion("item_gold_coin");
        }
    }

    public class AssetLevelDecoration {
        public final AtlasRegion chemSymbol;
        public final AtlasRegion cloud;
        public final AtlasRegion matahari;
        public final AtlasRegion mountain1;
        public final AtlasRegion mountain2;
        public final AtlasRegion mountain3;
        public final AtlasRegion pipa;
        public final AtlasRegion portal;
        public final AtlasRegion semak;
        public final AtlasRegion waterOverlay;

        public AssetLevelDecoration(TextureAtlas atlas) {
            this.cloud = atlas.findRegion("cloud");
            this.mountain1 = atlas.findRegion("mountain1");
            this.mountain2 = atlas.findRegion("mountain2");
            this.mountain3 = atlas.findRegion("mountain3");
            this.waterOverlay = atlas.findRegion("water_overlay");
            this.portal = atlas.findRegion("portal");
            this.chemSymbol = atlas.findRegion("chemsymbol");
            this.matahari = atlas.findRegion("matahari");
            this.pipa = atlas.findRegion("pipa");
            this.semak = atlas.findRegion("semak");
        }
    }

    public class AssetMusic {
        public final Music song1;

        public AssetMusic(AssetManager am) {
            this.song1 = (Music) am.get("music/keith303_-_brand_new_highscore.mp3", Music.class);
        }
    }

    public class AssetPad {
        public final AtlasRegion padJump;
        public final AtlasRegion padLeft;
        public final AtlasRegion padRight;

        public AssetPad(TextureAtlas atlas) {
            this.padLeft = atlas.findRegion("padLeft");
            this.padRight = atlas.findRegion("padRight");
            this.padJump = atlas.findRegion("padJump");
        }
    }

    public class AssetPersonConversation {
        public final Array<String> imagePath = new Array();
        public final Array<AtlasRegion> imagePerson = new Array();

        public AssetPersonConversation(TextureAtlas atlas) {
        }
    }

    public class AssetRock {
        public final AtlasRegion edge;
        public final AtlasRegion middle;

        public AssetRock(TextureAtlas atlas) {
            this.edge = atlas.findRegion("rockedge");
            this.middle = atlas.findRegion("rockmiddle");
        }
    }

    public class AssetSoal {
        public final AtlasRegion pembagi;
        public final AtlasRegion soal;

        public AssetSoal(TextureAtlas atlas) {
            this.soal = atlas.findRegion("soalfix");
            this.pembagi = atlas.findRegion("pembagi");
        }
    }

    public class AssetSoalLatihan {
        public ArrayMap<String, AtlasRegion> soal = new ArrayMap();

        public AssetSoalLatihan(TextureAtlas atlas) {
            this.soal.put("soal12", atlas.findRegion("soal12"));
            this.soal.put("soal13", atlas.findRegion("soal13"));
            this.soal.put("soal23", atlas.findRegion("soal23"));
            this.soal.put("soal31", atlas.findRegion("soal31"));
            this.soal.put("soal32", atlas.findRegion("soal32"));
        }
    }

    public class AssetSounds {
        public final Sound jump;
        public final Sound liveLost;
        public final Sound pickupCoin;

        public AssetSounds(AssetManager am) {
            this.jump = (Sound) am.get("sounds/jump.wav", Sound.class);
            this.pickupCoin = (Sound) am.get("sounds/pickup_coin.wav", Sound.class);
            this.liveLost = (Sound) am.get("sounds/live_lost.wav", Sound.class);
        }
    }

    public class AssetMenuInGame {

        public AtlasRegion panel_pause;
        public AtlasRegion panel_bantuan;
        public Skin menuInGame;

        AssetMenuInGame(TextureAtlas atlas){
            panel_pause = atlas.findRegion("pause_panel");
            panel_bantuan = atlas.findRegion("bantuan_panel");
            menuInGame = new Skin(Gdx.files.internal(Constants.SKIN_MENU_IN_GAME), atlas);
        }
    }

    public class AssetPanduan {
        public TeksPanduan panduan;

        AssetPanduan(){
            panduan = new Json().fromJson(TeksPanduan.class, Gdx.files.internal(Constants.DATA_PANDUAN_JSON));
        }
    }

    private Assets() {
    }

    public void init(AssetManager assetManager) {
        this.assetManager = assetManager;
        assetManager.setErrorListener(this);
        assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_UI, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_BOOK, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_BOOK_CONTENT, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_FORMULA_CONTENT, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_SOAL, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_KARAKTER, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_SOAL_LATIHAN, TextureAtlas.class);
        assetManager.load(Constants.TEXTURE_ATLAS_MENU_IN_GAME, TextureAtlas.class);
        assetManager.load("sounds/jump.wav", Sound.class);
        assetManager.load("sounds/pickup_coin.wav", Sound.class);
        assetManager.load("sounds/live_lost.wav", Sound.class);
        assetManager.load("music/keith303_-_brand_new_highscore.mp3", Music.class);
        assetManager.finishLoading();
        Gdx.app.debug(TAG, "# of assets loaded: " + assetManager.getAssetNames().size);
        Iterator it = assetManager.getAssetNames().iterator();
        while (it.hasNext()) {
            Gdx.app.debug(TAG, "asset: " + ((String) it.next()));
        }
        TextureAtlas atlas = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);
        TextureAtlas atlasUI = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_UI);
        TextureAtlas atlasBook = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_BOOK);
        TextureAtlas atlasContentBook = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_BOOK_CONTENT);
        TextureAtlas atlasFormulaContent = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_FORMULA_CONTENT);
        TextureAtlas atlasSoal = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_SOAL);
        TextureAtlas atlasKarakter = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_KARAKTER);
        TextureAtlas atlasSoalLatihan = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_SOAL_LATIHAN);
        TextureAtlas menuInGameAtlas = (TextureAtlas) assetManager.get(Constants.TEXTURE_ATLAS_MENU_IN_GAME);
        this.skinLibGdx = new Skin(Gdx.files.internal(Constants.SKIN_LIBGDX_UI), new TextureAtlas(Constants.TEXTURE_ATLAS_LIBGDX_UI));
        this.bunny = new AssetBunny(atlas);
        this.rock = new AssetRock(atlas);
        this.goldCoin = new AssetGoldCoin(atlas);
        this.feather = new AssetFeather(atlas);
        this.levelDecoration = new AssetLevelDecoration(atlas);
        this.pads = new AssetPad(atlasUI);
        this.karakter = new AssetCharacter(atlasKarakter);
        this.assetBook = new AssetBook(atlasBook);
        this.assetBookContent = new AssetBookContent(atlasContentBook);
        this.assetFormulaContent = new AssetFormulaContent(atlasFormulaContent);
        this.fonts = new AssetFonts();
        this.sounds = new AssetSounds(assetManager);
        this.music = new AssetMusic(assetManager);
        this.dataConversation = new AssetConversation();
        this.soalLevel3 = new AssetSoal(atlasSoal);
        this.soalLatihan = new AssetSoalLatihan(atlasSoalLatihan);
        this.builder = new ConversationPanelBuilder();
        this.mig = new AssetMenuInGame(menuInGameAtlas);
        this.panduan = new AssetPanduan();
    }

    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception) throwable);
    }

    public void dispose() {
        this.assetManager.dispose();
        this.fonts.defaultSmall.dispose();
        this.fonts.defaultNormal.dispose();
        this.fonts.defaultBig.dispose();
    }
}
