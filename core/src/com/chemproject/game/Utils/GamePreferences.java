package com.chemproject.game.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;
import com.chemproject.game.Utils.Constants.PreferenceConstants;

public class GamePreferences {
    public static final String TAG = GamePreferences.class.getName();
    public static final GamePreferences instance = new GamePreferences();
    public int charSkin;
    public boolean music;
    private Preferences prefs = Gdx.app.getPreferences(Constants.PREFERENCES);
    public boolean showFpsCounter;
    public boolean sound;
    public float volMusic;
    public float volSound;

    private GamePreferences() {
    }

    public void load() {
        this.sound = this.prefs.getBoolean(PreferenceConstants.SOUND_PREFERENCES, true);
        this.music = this.prefs.getBoolean(PreferenceConstants.MUSIC_PREFERENCES, true);
        this.volSound = MathUtils.clamp(this.prefs.getFloat(PreferenceConstants.VOLUME_SOUND_PREFERENCES, 0.5f), 0.0f, 1.0f);
        this.volMusic = MathUtils.clamp(this.prefs.getFloat(PreferenceConstants.VOLUME_MUSIC_PREFERENCES, 0.5f), 0.0f, 1.0f);
        this.charSkin = MathUtils.clamp(this.prefs.getInteger(PreferenceConstants.CHAR_SKIN_PREFERENCES, 0), 0, 2);
        this.showFpsCounter = this.prefs.getBoolean(PreferenceConstants.SHOW_FPS_COUNTER_PREFERENCES, false);
    }

    public void save() {
        this.prefs.putBoolean(PreferenceConstants.SOUND_PREFERENCES, this.sound);
        this.prefs.putBoolean(PreferenceConstants.MUSIC_PREFERENCES, this.music);
        this.prefs.putFloat(PreferenceConstants.VOLUME_SOUND_PREFERENCES, this.volSound);
        this.prefs.putFloat(PreferenceConstants.VOLUME_MUSIC_PREFERENCES, this.volMusic);
        this.prefs.putInteger(PreferenceConstants.CHAR_SKIN_PREFERENCES, this.charSkin);
        this.prefs.putBoolean(PreferenceConstants.SHOW_FPS_COUNTER_PREFERENCES, this.showFpsCounter);
        this.prefs.flush();
    }
}
