package com.chemproject.game.Utils;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.chemproject.game.ObjectGame.AbstractGameObject;

public class CameraHelper {
    public static final String TAG = CameraHelper.class.getName();
    private final float FOLLOW_SPEED = 4.0f;
    private final float MAX_ZOOM_IN = 0.25f;
    private final float MAX_ZOOM_OUT = 10.0f;
    private Vector2 position = new Vector2();
    private AbstractGameObject target;
    private float zoom = 1.0f;

    public void update(float deltaTime) {
        if (hasTarget()) {
            this.position.lerp(this.target.position, 4.0f * deltaTime);
            this.position.y = Math.max(-1.0f, this.position.y);
        }
    }

    public void setPosition(float x, float y) {
        this.position.set(x, y);
    }

    public Vector2 getPosition() {
        return this.position;
    }

    public void addZoom(float amount) {
        setZoom(this.zoom + amount);
    }

    public void setZoom(float zoom) {
        this.zoom = MathUtils.clamp(zoom, 0.25f, 10.0f);
    }

    public float getZoom() {
        return this.zoom;
    }

    public void setTarget(AbstractGameObject target) {
        this.target = target;
    }

    public AbstractGameObject getTarget() {
        return this.target;
    }

    public boolean hasTarget() {
        return this.target != null;
    }

    public boolean hasTarget(AbstractGameObject target) {
        return hasTarget() && this.target.equals(target);
    }

    public void applyTo(OrthographicCamera camera) {
        camera.position.x = this.position.x;
        camera.position.y = this.position.y;
        camera.zoom = this.zoom;
        camera.update();
    }
}
