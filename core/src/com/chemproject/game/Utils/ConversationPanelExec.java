package com.chemproject.game.Utils;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.Model.Conversation;
import com.chemproject.game.Model.SceneConversation;
import com.chemproject.game.Screens.Panel.ConversationPanel;
import java.util.Iterator;

public class ConversationPanelExec {
    private Image background;
    private int indeksConversation;
    private int indeksTemp;
    private boolean isConversationBegin;
    public boolean isConversationDone;
    private Array<ConversationPanel> panels;
    private SceneConversation scene;
    private Stack stack;
    private Stage stage;

    public void init() {
        this.indeksConversation = -1;
        this.indeksTemp = 0;
        this.isConversationDone = true;
        this.isConversationBegin = false;
        this.background = new Image(Assets.instance.assetBook.bgDialog);
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        this.stack = new Stack();
        this.stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.stack.add(this.background);
        this.stage.addActor(this.stack);
    }

    private void update() {
        if (this.indeksTemp != this.indeksConversation) {
            this.stack.addActor((Actor) this.panels.get(this.indeksTemp));
            this.indeksConversation++;
        }
        if (((ConversationPanel) this.panels.get(this.indeksTemp)).isTextDoneWriting) {
            ((ConversationPanel) this.panels.get(this.indeksTemp)).panelDone();
            this.indeksTemp++;
        } else {
            ((ConversationPanel) this.panels.get(this.indeksTemp)).updateText();
        }
        if (this.indeksTemp >= this.panels.size) {
            this.isConversationDone = true;
        }
    }

    public void render() {
        if (!this.isConversationDone) {
            if (this.isConversationBegin) {
                update();
            }
            this.stage.draw();
        }
    }

    public void setPanels(String key) {
        this.scene = Assets.instance.builder.getConversationPanels(key);
        this.panels = new Array();
        this.indeksConversation = -1;
        this.indeksTemp = 0;
        this.isConversationDone = false;
        Iterator it = this.scene.getConversations().iterator();
        while (it.hasNext()) {
            Conversation convers = (Conversation) it.next();
            this.panels.add(new ConversationPanel(convers.getTextConversation(), convers.getNamePerson(), convers.getObjectConversation(), convers.getPanelAlign(), convers.getImagePerson()));
        }
    }

    public void resize(float width, float height) {
        this.stage.getViewport().update((int) width, (int) height);
    }

    public void dispose() {
        this.stage.dispose();
    }

    public Stage getInputProcessor() {
        return this.stage;
    }

    public void beginConversation(Image background) {
        this.isConversationBegin = true;
        background.setDrawable(background.getDrawable());
    }
}
