package com.chemproject.game.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Json;
import com.chemproject.game.Model.ConversationData;
import com.chemproject.game.Model.ConversationData.Data;
import com.chemproject.game.Model.SceneConversation;
import java.util.Iterator;

public class ConversationPanelBuilder {
    private ArrayMap<String, SceneConversation> conversations = new ArrayMap();
    private ConversationData data = Assets.instance.dataConversation.conversation;

    public ConversationPanelBuilder() {
        init();
    }

    private void init() {
        Json json = new Json();
        Iterator it = this.data.getDataJsonConversation().iterator();
        while (it.hasNext()) {
            SceneConversation temp = (SceneConversation) json.fromJson(SceneConversation.class, Gdx.files.internal(((Data) it.next()).getJsonConversation()));
            this.conversations.put(temp.getSceneName(), temp);
        }
    }

    public SceneConversation getConversationPanels(String key) {
        return (SceneConversation) this.conversations.get(key);
    }
}
