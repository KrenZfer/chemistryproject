package com.chemproject.game.Screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.chemproject.game.Utils.Assets;

public abstract class AbstractGameScreen implements Screen {
    protected DirectedGame game;

    public abstract InputProcessor getInputProcessor();

    public abstract void hide();

    public abstract void pause();

    public abstract void render(float f);

    public abstract void resize(int i, int i2);

    public abstract void show();

    public AbstractGameScreen(DirectedGame game) {
        this.game = game;
    }

    public void resume() {
        Assets.instance.init(new AssetManager());
    }

    public void dispose() {
        Assets.instance.dispose();
    }
}
