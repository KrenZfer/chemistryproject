package com.chemproject.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;

import java.text.DecimalFormat;

public class FinalScreen extends AbstractGameScreen {
    public static final String TAG = Level3Screen.class.getName();
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private final int TIME_COUNTDOWN = 50;
    private boolean debugEnabled = false;
    private float debugRebuildStage;
    private boolean isGameOver = false;
    private Label scoreLevel1;
    private Label scoreLevel2;
    private Label scoreLevel3;
    private Stage stage;
    private LabelStyle style;
    private Table tablePenilaianAkhir;

    public FinalScreen(DirectedGame game) {
        super(game);
    }

    public void render(float deltaTime) {
        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (this.debugEnabled) {
            this.debugRebuildStage -= deltaTime;
            if (this.debugRebuildStage <= 0.0f) {
                this.debugRebuildStage = 5.0f;
                rebuildStage();
            }
        }
        this.stage.act(deltaTime);
        this.stage.draw();
        this.stage.setDebugAll(this.debugEnabled);
    }

    private void rebuildStage() {
        new Stack().setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.style = new LabelStyle(Assets.instance.fonts.defaultBig, Color.WHITE);
        this.tablePenilaianAkhir = buildTablePenilaianAkhir();
        this.stage.addActor(this.tablePenilaianAkhir);
    }

    private Table buildTablePenilaianAkhir() {
        Table table = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        table.setBackground(new Image(Assets.instance.assetBook.background).getDrawable());
        table.row();
        Actor lbljdl = new Label((CharSequence) "Level 1 ", this.style);
        this.scoreLevel1 = new Label(new DecimalFormat("##.##").format((double) Constants.finalScoreLevel1) + "%", this.style);
        table.add(lbljdl).align(8).pad(5.0f);
        table.add(this.scoreLevel1).pad(5.0f).align(16);
        table.row();
        lbljdl = new Label((CharSequence) "Level 2 ", this.style);
        this.scoreLevel2 = new Label(new DecimalFormat("##.##").format((double) Constants.finalScoreLevel2) + "%", this.style);
        table.add(lbljdl).align(8).pad(5.0f);
        table.add(this.scoreLevel2).pad(5.0f).align(16);
        table.row();
        lbljdl = new Label((CharSequence) "Level 3 ", this.style);
        this.scoreLevel3 = new Label(new DecimalFormat("##.##").format((double) Constants.finalScoreLevel3) + "%", this.style);
        table.add(lbljdl).align(8).pad(5.0f);
        table.add(this.scoreLevel3).pad(5.0f).align(16);
        table.row();
        lbljdl = new Label((CharSequence) "NILAI AKHIR ", this.style);
        Actor finalScorelbl = new Label(new DecimalFormat("##.##").format((double) (((Constants.finalScoreLevel1 + Constants.finalScoreLevel2) + Constants.finalScoreLevel3) / 3.0f)) + "%", this.style);
        table.add(lbljdl).align(8).pad(5.0f);
        table.add(finalScorelbl).pad(5.0f).align(16);
        Actor btn = new TextButton("FINISH", Assets.instance.skinLibGdx);
        btn.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                goToProfil();
            }
        });
        table.row();
        table.add(btn).size(70.0f, 50.0f).colspan(2).pad(10.0f, 10.0f, 100.0f, 10.0f);
        return table;
    }

    private void goToProfil(){
        game.setScreen(new ProfilScreen(game));
    }

    private void update(float deltaTime) {
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height);
    }

    public void show() {
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        rebuildStage();
    }

    public void hide() {
        this.stage.dispose();
    }

    public void pause() {
    }

    public InputProcessor getInputProcessor() {
        return this.stage;
    }
}
