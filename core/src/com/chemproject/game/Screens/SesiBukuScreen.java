package com.chemproject.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.ChemistryProject;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.TextRenderer;

public class SesiBukuScreen extends AbstractGameScreen {
    public static final String TAG = AbstractGameScreen.class.getName();
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private Image background;
    private Table backgroundTable;
    private Button bioBook;
    private Button chemBook;
    private Button closeButton;
    private boolean debugEnabled = false;
    private float debugRebuildStage;
    private Image firstPage;
    private Button folkloreBook;
    private boolean isChemBookOpened;
    private boolean isFirstPage;
    private int level;
    private Button nextPage;
    private AbstractGameScreen nextScreen;
    private Image openBook;
    private Table openBookTable;
    private Image secondPage;
    private Image shelf;
    private Table shelfTable;
    private Stage stage;

    public SesiBukuScreen(DirectedGame game, int level, AbstractGameScreen nextScreen) {
        super(game);
        this.level = level;
        this.isFirstPage = true;
        this.nextScreen = nextScreen;
    }

    private void rebuildStage() {
        this.backgroundTable = buildTableBackground();
        this.shelfTable = buildTableBookShelf();
        this.openBookTable = buildTableOpenBook();
        this.openBookTable.setVisible(false);
        this.stage.clear();
        Stack stack = new Stack();
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.stage.addActor(stack);
        stack.add(this.backgroundTable);
        stack.add(this.shelfTable);
        stack.add(this.openBookTable);
    }

    private Table buildTableBackground() {
        Table table = new Table();
        this.background = new Image(Assets.instance.assetBook.background);
        table.background(this.background.getDrawable());
        return table;
    }

    private Table buildTableBookShelf() {
        Table table = new Table();
        this.shelf = new Image(Assets.instance.assetBook.bookShelf);
        this.bioBook = new Button(new Image(Assets.instance.assetBook.bioBook).getDrawable());
        this.bioBook.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                SesiBukuScreen.this.isChemBookOpened = false;
                SesiBukuScreen.this.showBook();
            }
        });
        this.chemBook = new Button(new Image(Assets.instance.assetBook.chemBook).getDrawable());
        this.chemBook.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                SesiBukuScreen.this.isChemBookOpened = true;
                SesiBukuScreen.this.showBook();
            }
        });
        this.folkloreBook = new Button(new Image(Assets.instance.assetBook.folkloreBook).getDrawable());
        this.folkloreBook.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                SesiBukuScreen.this.isChemBookOpened = false;
                SesiBukuScreen.this.showBook();
            }
        });
        Stack stackShelf = new Stack();
        stackShelf.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        table.add(stackShelf);
        Table shelfTable = new Table();
        shelfTable.add(this.shelf).size(Constants.getRealWidth(1000.0f), Constants.getRealHeight(600.0f));
        Table bookTable = new Table();
        bookTable.left().top().add(this.chemBook).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(200.0f)).pad(Constants.getRealHeight(75.0f), Constants.getRealWidth(40.0f), 0.0f, 0.0f);
        bookTable.row();
        bookTable.left().top().add(this.bioBook).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(200.0f)).pad(Constants.getRealHeight(75.0f), Constants.getRealWidth(40.0f), 0.0f, 0.0f);
        bookTable.add(this.folkloreBook).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(200.0f)).expandX().right().pad(Constants.getRealHeight(75.0f), 0.0f, 0.0f, Constants.getRealWidth(40.0f));
        stackShelf.add(shelfTable);
        stackShelf.add(bookTable);
        return table;
    }

    private Table buildTableOpenBook() {
        Table table = new Table();
        Table tableBook = new Table();
        Table tableContent = new Table();
        Table tableRefference = new Table();
        this.openBook = new Image(Assets.instance.assetBook.openBook);
        this.closeButton = new Button(new Image(Assets.instance.assetBook.closeButton).getDrawable());
        this.nextPage = new Button(new Image(Assets.instance.assetBook.nextPage).getDrawable());
        this.firstPage = new Image();
        this.secondPage = new Image();
        Stack stack = new Stack();
        stack.setSize(Constants.getRealWidth(1280.0f), Constants.getRealHeight(720.0f));
        tableRefference.setSize(Constants.getRealWidth(1280.0f), Constants.getRealHeight(720.0f));
        tableBook.setSize(Constants.getRealWidth(1280.0f), Constants.getRealHeight(720.0f));
        this.closeButton.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                SesiBukuScreen.this.closeBook();
            }
        });
        this.nextPage.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                if (!SesiBukuScreen.this.isFirstPage || SesiBukuScreen.this.level == 1) {
                    SesiBukuScreen.this.game.setScreen(new LevelLatihanScreen(SesiBukuScreen.this.game, SesiBukuScreen.this.level, SesiBukuScreen.this.nextScreen), Constants.screenSlide);
                    return;
                }
                SesiBukuScreen.this.isFirstPage = false;
                SesiBukuScreen.this.chooseBook();
            }
        });
        tableBook.add(this.openBook).size(Constants.getRealWidth(1280.0f), Constants.getRealHeight(720.0f));
        tableContent.setSize(tableBook.getWidth(), tableBook.getMinHeight());
        tableContent.top().add(this.closeButton).size(Constants.getRealWidth(50.0f), Constants.getRealHeight(50.0f)).colspan(2).align(16).pad(Constants.getRealHeight(50.0f), 0.0f, Constants.getRealHeight(10.0f), 0.0f);
        tableContent.row();
        tableContent.add(this.firstPage).size(Constants.getRealWidth(500.0f), Constants.getRealHeight(500.0f)).pad(0.0f, 0.0f, 0.0f, Constants.getRealWidth(70.0f)).align(8);
        tableContent.add(this.secondPage).size(Constants.getRealWidth(500.0f), Constants.getRealHeight(500.0f)).pad(0.0f, Constants.getRealWidth(70.0f), 0.0f, 0.0f).align(16);
        tableContent.row();
        tableContent.add(this.nextPage).size(Constants.getRealWidth(50.0f), Constants.getRealHeight(50.0f)).colspan(2).align(16).pad(0.0f, 0.0f, Constants.getRealHeight(50.0f), 0.0f);
        tableRefference.bottom().left();
        tableRefference.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "Untuk lebih jelasnya kalian bisa membuka @Sumber 1=https://www.zenius.net/c/5141/larutan-buffer@ atau @Sumber 2=https://www.bisakimia.com/2012/11/21/buffer-larutan-penyangga/@", Color.BLACK)).pad(0.0f, Constants.getRealWidth(60.0f), Constants.getRealHeight(50.0f), 0.0f);
        stack.add(tableBook);
        stack.add(tableContent);
        stack.add(tableRefference);
        table.add(stack);
        return table;
    }

    private void chooseBook() {
        this.firstPage.setVisible(true);
        this.secondPage.setVisible(true);
        this.closeButton.setVisible(false);
        this.nextPage.setVisible(true);
        if (this.level == 1) {
            this.firstPage.setDrawable(new Image(Assets.instance.assetBookContent.level1).getDrawable());
            this.secondPage.setVisible(false);
        } else if (this.level == 2) {
            if (this.isFirstPage) {
                this.firstPage.setDrawable(new Image(Assets.instance.assetBookContent.level21).getDrawable());
                this.secondPage.setDrawable(new Image(Assets.instance.assetBookContent.level22).getDrawable());
                return;
            }
            this.firstPage.setDrawable(new Image(Assets.instance.assetBookContent.level23).getDrawable());
            this.secondPage.setVisible(false);
        } else if (this.level != 3) {
        } else {
            if (this.isFirstPage) {
                this.firstPage.setDrawable(new Image(Assets.instance.assetBookContent.level31).getDrawable());
                this.secondPage.setDrawable(new Image(Assets.instance.assetBookContent.level32).getDrawable());
                return;
            }
            this.firstPage.setDrawable(new Image(Assets.instance.assetBookContent.level33).getDrawable());
            this.secondPage.setVisible(false);
        }
    }

    private void closeBook() {
        this.openBookTable.setVisible(false);
        this.isFirstPage = true;
    }

    private void showBook() {
        this.shelfTable.setVisible(false);
        this.openBookTable.setVisible(true);
        if (this.isChemBookOpened) {
            chooseBook();
        } else {
            showEmptyBook();
        }
    }

    private void showEmptyBook() {
        this.openBookTable.setVisible(true);
        this.firstPage.setVisible(false);
        this.secondPage.setVisible(false);
        this.nextPage.setVisible(false);
        this.closeButton.setVisible(true);
    }

    public void render(float deltaTime) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (this.debugEnabled) {
            this.debugRebuildStage -= deltaTime;
            if (this.debugRebuildStage <= 0.0f) {
                this.debugRebuildStage = 5.0f;
                rebuildStage();
            }
        }
        this.stage.act(deltaTime);
        this.stage.draw();
        this.stage.setDebugTableUnderMouse(this.debugEnabled);
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height, true);
    }

    public void show() {
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        rebuildStage();
    }

    public void hide() {
        this.stage.dispose();
    }

    public void pause() {
    }

    public InputProcessor getInputProcessor() {
        return this.stage;
    }
}
