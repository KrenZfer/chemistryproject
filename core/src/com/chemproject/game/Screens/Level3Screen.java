package com.chemproject.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.ChemistryProject;
import com.chemproject.game.Screens.Panel.UIGame;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.ConversationPanelExec;
import com.chemproject.game.Utils.TextRenderer;

import java.text.DecimalFormat;

public class Level3Screen extends AbstractGameScreen {
    public static final String TAG = Level3Screen.class.getName();
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private final int TIME_COUNTDOWN = 50;
    private ConversationPanelExec conver = new ConversationPanelExec();
    private STATE currentState = STATE.PLAY;
    private boolean debugEnabled = false;
    private float debugRebuildStage;
    private float fieldHeight = Constants.getRealHeight(30.0f);
    private float fieldLengthBig = Constants.getRealWidth(150.0f);
    private float fieldLengthSmall = Constants.getRealWidth(50.0f);
    private boolean isGameOver = false;
    private boolean isPaused;
    private boolean islangkah1True;
    private boolean islangkah2True;
    private TextField jawaban11;
    private TextField jawaban12;
    private TextField jawaban13;
    private TextField jawaban14;
    private TextField jawaban21;
    private TextField jawaban22;
    private TextField jawaban23;
    private TextField jawaban24;
    private TextField jawaban31;
    private TextField jawaban32;
    private TextField jawaban33;
    private Label labelKeputusan;
    private Label labelNilai;
    private TextButton langkah1Btn;
    private TextButton langkah2Btn;
    private TextButton langkah3Btn;
    private float nilaiAkhir = 0.0f;
    private Stage stage;
    private Table tableLangkah1;
    private Table tableLangkah2;
    private Table tableLangkah3;
    private Table tableMain;
    private Table tablePenilaian;
    private Table tableSoal;
    private float timeLeftGameOverDelay;
    private SpriteBatch batch;
    private OrthographicCamera cameraGUI;
    private UIGame uiGame;

    enum STATE {
        PLAY,
        LOSE,
        WIN
    }

    public Level3Screen(DirectedGame game) {
        super(game);
        this.conver.init();
    }

    public void render(float deltaTime) {
        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (!this.isPaused
                && !uiGame.onPause
                ) {
            if (this.conver.isConversationDone) {
                update(deltaTime);
                Gdx.input.setInputProcessor(this.stage);
            } else {
                Gdx.input.setInputProcessor(this.conver.getInputProcessor());
            }
        }
        if (uiGame.toMenu)
            backToMenu();
        if (this.debugEnabled) {
            this.debugRebuildStage -= deltaTime;
            if (this.debugRebuildStage <= 0.0f) {
                this.debugRebuildStage = 5.0f;
                rebuildStage();
            }
        }
        this.stage.act(deltaTime);
        this.stage.draw();
        uiGame.draw();
        this.stage.setDebugAll(this.debugEnabled);
        if (this.conver.isConversationDone) {
            this.isPaused = false;
            return;
        }
        this.conver.render();
        if (this.isPaused) {
            this.conver.beginConversation(new Image(Assets.instance.assetBook.background));
        }
    }

    private void initLevel() {
        this.islangkah1True = false;
        this.islangkah2True = false;
        this.isPaused = true;
        this.timeLeftGameOverDelay = 3.0f;
        this.nilaiAkhir = 0.0f;
        this.conver.setPanels("level3");
        this.currentState = STATE.PLAY;
    }

    private void update(float deltaTime) {
        this.tableLangkah2.setVisible(this.islangkah1True);
        this.tableLangkah3.setVisible(this.islangkah2True);
        if (this.currentState != STATE.PLAY) {
            this.timeLeftGameOverDelay -= deltaTime;
            if (this.timeLeftGameOverDelay < 0.0f) {
                if (this.currentState == STATE.LOSE) {
                    restart();
                } else if (this.currentState == STATE.WIN) {
                    continueToSesiBuku();
                }
                this.timeLeftGameOverDelay = 3.0f;
            }
            if (this.currentState == STATE.WIN) {
                this.labelKeputusan.setText("Kamu Lulus!!!!");
                this.labelKeputusan.getStyle().fontColor = Color.GREEN;
                this.labelNilai.getStyle().fontColor = Color.GREEN;
            } else if (this.currentState == STATE.LOSE) {
                this.labelKeputusan.setText("Kamu Gagal!!!!");
                this.labelKeputusan.getStyle().fontColor = Color.RED;
                this.labelNilai.getStyle().fontColor = Color.RED;
            }
            this.labelNilai.setText(new DecimalFormat("##.##").format((double) this.nilaiAkhir) + "%");
            this.tablePenilaian.setVisible(true);
            return;
        }
        this.tablePenilaian.setVisible(false);
    }

    private void restart() {
        this.game.setScreen(new Level3Screen(this.game), Constants.screenSlide);
    }

    private void continueToSesiBuku() {
        this.game.setScreen(new SesiBukuScreen(this.game, 3, new FinalScreen(this.game)), Constants.screenSlide);
    }

    private void rebuildStage() {
        uiGame.init(3);
        Stack stack = new Stack();
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.tableMain = new Table();
        this.tableMain.setSize(this.stage.getWidth(), this.stage.getHeight());
        this.tableMain.background(new Image(Assets.instance.assetBook.background).getDrawable());
        this.tableSoal = buildTableSoal();
        this.tableLangkah1 = buildTableLangkah(1);
        this.tableLangkah2 = buildTableLangkah(2);
        this.tableLangkah3 = buildTableLangkah(3);
        this.tablePenilaian = buildTablePenilaian();
        this.tablePenilaian.setVisible(false);
        this.tableLangkah2.setVisible(false);
        this.tableLangkah3.setVisible(false);
        this.tableMain.top().left().padTop(Constants.getRealHeight(100.0f));
        this.tableMain.add(this.tableSoal).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(30.0f), Constants.getRealHeight(10.0f), Constants.getRealWidth(30.0f)).colspan(3);
        this.tableMain.row().left();
        this.tableMain.add(this.tableLangkah1).size(Constants.getRealWidth(400.0f), Constants.getRealHeight(400.0f)).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(30.0f), Constants.getRealHeight(10.0f), 0.0f);
        this.tableMain.add(this.tableLangkah2).size(Constants.getRealWidth(400.0f), Constants.getRealHeight(400.0f)).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(10.0f), Constants.getRealHeight(10.0f), 0.0f);
        this.tableMain.add(this.tableLangkah3).size(Constants.getRealWidth(400.0f), Constants.getRealHeight(400.0f)).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(10.0f), Constants.getRealHeight(10.0f), Constants.getRealWidth(30.0f));
        stack.add(this.tableMain);
        stack.add(this.tablePenilaian);
        this.stage.addActor(stack);
    }

    private Table buildTablePenilaian() {
        Table table = new Table();
        Pixmap pixmap = new Pixmap((int) Constants.VIEWPORT_GUI_WIDTH, (int) Constants.VIEWPORT_GUI_HEIGHT, Format.RGBA8888);
        pixmap.setColor(0.5f, 0.5f, 0.5f, 0.5f);
        pixmap.fillRectangle(0, 0, (int) Constants.VIEWPORT_GUI_WIDTH, (int) Constants.VIEWPORT_GUI_HEIGHT);
        table.setBackground(new Image(new Texture(pixmap)).getDrawable());
        LabelStyle style = new LabelStyle(Assets.instance.fonts.defaultBig, Color.GREEN);
        this.labelKeputusan = new Label((CharSequence) "Kamu Lulus!!!!", style);
        table.add(this.labelKeputusan);
        table.row();
        this.labelNilai = new Label(this.nilaiAkhir + "%", style);
        table.add(this.labelNilai);
        return table;
    }

    private Table buildTableLangkah(int langkah) {
        Table table = new Table();
        table.setSize(Constants.getRealWidth(400.0f), Constants.getRealHeight(400.0f));
        table.background(new Image(Assets.instance.assetFormulaContent.pieceFrame).getDrawable());
        LabelStyle style = new LabelStyle(Assets.instance.fonts.defaultNormal, Color.BLACK);
        Actor textLangkah1 = new Label("Langkah " + langkah, style);
        Actor text = new Table();
        switch (langkah) {
            case 1:
                text = TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "Hitung mol masing-masing larutan", Color.BLACK);
                break;
            case 2:
                text = TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "Hitung Konsentrasi H^+^", Color.BLACK);
                break;
            case 3:
                text = TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "Hitung pH larutan", Color.BLACK);
                break;
        }
        table.left().top();
        table.add(textLangkah1).pad(Constants.getRealHeight(50.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f).colspan(4).align(8).expandX();
        table.row();
        table.add(text).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f).colspan(4).align(8);
        Table tempT;
        switch (langkah) {
            case 1:
                this.jawaban11 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban12 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban13 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban14 = new TextField("", Assets.instance.skinLibGdx);
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "n CH_3_COOH       = ", Color.BLACK)).align(16).pad(10.0f, 5.0f, 5.0f, 0.0f);
                table.add(this.jawaban11).size(this.fieldLengthSmall, this.fieldHeight).pad(Constants.getRealHeight(1.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(1.0f), 0.0f);
                table.add(new Label((CharSequence) "mL x 1 mol/L", style)).align(8);
                table.row();
                table.add(new Label((CharSequence) "= ", style)).align(16);
                table.add(this.jawaban12).size(this.fieldLengthSmall, this.fieldHeight).pad(Constants.getRealHeight(1.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(1.0f), 0.0f);
                table.add(new Label((CharSequence) "mmol ", style)).align(8);
                table.row();
                table.add();
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "n CH_3_COOK       = ", Color.BLACK)).align(16).pad(5.0f, 5.0f, 5.0f, 0.0f);
                table.add(this.jawaban13).size(this.fieldLengthSmall, this.fieldHeight).pad(Constants.getRealHeight(1.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(1.0f), 0.0f);
                table.add(new Label((CharSequence) "mL x 1 mol/L", style)).align(8);
                table.row();
                table.add(new Label((CharSequence) "= ", style)).align(16);
                table.add(this.jawaban14).size(this.fieldLengthSmall, this.fieldHeight).pad(Constants.getRealHeight(1.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(1.0f), 0.0f);
                table.add(new Label((CharSequence) "mmol ", style)).align(8);
                table.row();
                this.langkah1Btn = new TextButton("Lanjut", Assets.instance.skinLibGdx);
                this.langkah1Btn.addListener(new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        Level3Screen.this.islangkah1True = true;
                    }
                });
                table.add(this.langkah1Btn).padTop(10.0f).colspan(4).padTop(Constants.getRealHeight(20.0f));
                break;
            case 2:
                this.jawaban21 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban21.setAlignment(1);
                this.jawaban22 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban22.setAlignment(1);
                this.jawaban23 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban24 = new TextField("", Assets.instance.skinLibGdx);
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "[H^+^]                 = ", Color.BLACK)).align(16).pad(5.0f, 5.0f, 5.0f, 0.0f);
                tempT = new Table();
                tempT.background(new Image(Assets.instance.soalLevel3.pembagi).getDrawable());
                tempT.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "mol Asam", Color.BLACK));
                tempT.row();
                tempT.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "mol Basa Konjugasi", Color.BLACK));
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "K_a_ x ", Color.BLACK));
                table.add(tempT).pad(Constants.getRealHeight(1.0f), Constants.getRealWidth(10.0f), Constants.getRealHeight(1.0f), 0.0f).align(8).colspan(2);
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, " = ", Color.BLACK)).align(16).pad(Constants.getRealHeight(20.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f);
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "10^-5^ x ", Color.BLACK)).align(16).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f);
                tempT = new Table();
                tempT.background(new Image(Assets.instance.soalLevel3.pembagi).getDrawable());
                tempT.add(this.jawaban21).size(this.fieldLengthBig, this.fieldHeight).pad(0.0f, 0.0f, Constants.getRealHeight(5.0f), 0.0f);
                tempT.row();
                tempT.add(this.jawaban22).size(this.fieldLengthBig, this.fieldHeight).pad(Constants.getRealHeight(5.0f), 0.0f, 0.0f, 0.0f);
                table.add(tempT).size(this.fieldLengthBig, this.fieldHeight).pad(Constants.getRealHeight(20.0f), Constants.getRealWidth(10.0f), Constants.getRealHeight(20.0f), 0.0f).colspan(2).align(8);
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, " = ", Color.BLACK)).align(16).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f);
                table.add(this.jawaban23).size(this.fieldLengthSmall, this.fieldHeight).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(0.0f), Constants.getRealHeight(5.0f), 0.0f);
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "x 10 pangkat ", Color.BLACK)).align(8).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(0.0f), Constants.getRealHeight(5.0f), 0.0f);
                table.add(this.jawaban24).size(this.fieldLengthSmall, this.fieldHeight).align(8).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(0.0f), Constants.getRealHeight(5.0f), 0.0f).align(8);
                table.row();
                this.langkah2Btn = new TextButton("Lanjut", Assets.instance.skinLibGdx);
                this.langkah2Btn.addListener(new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        Level3Screen.this.islangkah2True = true;
                    }
                });
                table.add(this.langkah2Btn).colspan(4).padTop(Constants.getRealHeight(20.0f));
                break;
            case 3:
                this.jawaban31 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban32 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban33 = new TextField("", Assets.instance.skinLibGdx);
                this.jawaban33.setAlignment(1);
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, "pH = ", Color.BLACK)).align(16).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f);
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, " - Log[H^+^]", Color.BLACK)).align(8).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f).colspan(3);
                tempT = new Table();
                tempT.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, " - Log", Color.BLACK)).padRight(Constants.getRealWidth(5.0f));
                tempT.add(this.jawaban31).size(this.fieldLengthSmall, this.fieldHeight);
                tempT.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, " x 10 pangkat ", Color.BLACK)).padRight(Constants.getRealWidth(5.0f));
                tempT.add(this.jawaban32).size(this.fieldLengthSmall, this.fieldHeight);
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, " = ", Color.BLACK)).align(16).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f);
                table.add(tempT).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f).align(8);
                table.row();
                table.add(TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, " = ", Color.BLACK)).align(16).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f);
                table.add(this.jawaban33).size(this.fieldLengthBig, this.fieldHeight).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f).colspan(3).align(8);
                table.row();
                this.langkah3Btn = new TextButton("Selesai", Assets.instance.skinLibGdx);
                this.langkah3Btn.addListener(new ChangeListener() {
                    public void changed(ChangeEvent event, Actor actor) {
                        Level3Screen.this.checkAnswer();
                    }
                });
                table.add(this.langkah3Btn).colspan(4).padTop(Constants.getRealHeight(20.0f));
                break;
        }
        return table;
    }

    private void checkAnswer() {
        float nilai = 0.0f;
        if (this.jawaban11.getText().trim().equalsIgnoreCase("50")) {
            nilai = 0.0f + 1.0f;
        }
        if (this.jawaban12.getText().trim().equalsIgnoreCase("50")) {
            nilai += 1.0f;
        }
        if (this.jawaban13.getText().trim().equalsIgnoreCase("100")) {
            nilai += 1.0f;
        }
        if (this.jawaban14.getText().trim().equalsIgnoreCase("100")) {
            nilai += 1.0f;
        }
        if (this.jawaban21.getText().equalsIgnoreCase("50")) {
            nilai += 1.0f;
        }
        if (this.jawaban22.getText().equalsIgnoreCase("100")) {
            nilai += 1.0f;
        }
        if (this.jawaban23.getText().equalsIgnoreCase("5")) {
            nilai += 1.0f;
        }
        if (this.jawaban24.getText().equalsIgnoreCase("-6")) {
            nilai += 1.0f;
        }
        if (this.jawaban31.getText().equalsIgnoreCase("5")) {
            nilai += 1.0f;
        }
        if (this.jawaban32.getText().equalsIgnoreCase("-6")) {
            nilai += 1.0f;
        }
        if (this.jawaban33.getText().equalsIgnoreCase("6-Log5") || this.jawaban33.getText().equalsIgnoreCase("6 - Log5")) {
            nilai += 1.0f;
        }
        this.nilaiAkhir = (nilai / 11.0f) * 100.0f;
        Constants.finalScoreLevel3 = this.nilaiAkhir;
        if (this.currentState != STATE.PLAY) {
            return;
        }
        if (this.nilaiAkhir > 70.0f) {
            this.currentState = STATE.WIN;
        } else {
            this.currentState = STATE.LOSE;
        }
    }

    private Table buildTableSoal() {
        Table table = new Table();
        table.background(new Image(Assets.instance.assetFormulaContent.pieceFrame).getDrawable());
        table.add(new Image(Assets.instance.soalLevel3.soal));
        return table;
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height);
        this.conver.resize((float) width, (float) height);
        this.cameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
        this.cameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT / ((float) height)) * ((float) width);
        this.cameraGUI.position.set(this.cameraGUI.viewportWidth / 2.0f, this.cameraGUI.viewportHeight / 2.0f, 0.0f);
        this.cameraGUI.update();
        uiGame.resize(width, height);
    }

    public void show() {
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        batch = new SpriteBatch();
        this.cameraGUI = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
//        this.cameraGUI.position.set(0.0f, 0.0f, 0.0f);
//        this.cameraGUI.setToOrtho(true);
//        this.cameraGUI.update();
        uiGame = new UIGame(cameraGUI, batch);
        initLevel();
        rebuildStage();
    }

    public void hide() {
        this.stage.dispose();
        this.conver.dispose();
        uiGame.dispose();
    }

    public void pause() {
    }

    public void resume() {
        super.resume();
        this.isPaused = false;
    }

    public InputProcessor getInputProcessor() {
        if (this.conver.isConversationDone) {
            InputMultiplexer multiplexer = new InputMultiplexer();
            if (!uiGame.onPause)
                multiplexer.addProcessor(stage);
            multiplexer.addProcessor(uiGame.getInputProcessor());
            return multiplexer;
        }
        return this.conver.getInputProcessor();
    }

    public void backToMenu() {
        this.game.setScreen(new MenuScreen(this.game));
    }
}
