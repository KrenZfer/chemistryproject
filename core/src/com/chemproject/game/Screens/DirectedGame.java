package com.chemproject.game.Screens;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.chemproject.game.Screens.SreenTransition.IScreenTransition;

public class DirectedGame implements ApplicationListener {
    private SpriteBatch batch;
    private AbstractGameScreen currScreen;
    private boolean init;
    private float t;

    public void setScreen(AbstractGameScreen screen) {
        setScreen(screen, null);
    }

    public void setScreen(AbstractGameScreen screen, IScreenTransition screenTransition) {
        int w = Gdx.graphics.getWidth();
        int h = Gdx.graphics.getHeight();
        if (!this.init) {
            this.batch = new SpriteBatch();
            this.init = true;
        }
        this.currScreen = screen;
        this.currScreen.show();
        this.currScreen.resize(w, h);
        this.currScreen.render(0.0f);
        Gdx.input.setInputProcessor(this.currScreen.getInputProcessor());
    }

    @Override
    public void create() {
    }

    @Override
    public void resize(int width, int height) {
        if (this.currScreen != null) {
            this.currScreen.resize(width, height);
        }
    }

    @Override
    public void render() {
        this.currScreen.render(Math.min(Gdx.graphics.getDeltaTime(), 0.016666668f));
        Gdx.input.setInputProcessor(this.currScreen.getInputProcessor());
    }

    @Override
    public void pause() {
        if (this.currScreen != null) {
            this.currScreen.pause();
        }
    }

    @Override
    public void resume() {
        if (this.currScreen != null) {
            this.currScreen.resume();
        }
    }

    @Override
    public void dispose() {
        if (this.currScreen != null) {
            this.currScreen.hide();
        }
        if (this.init) {
            this.currScreen = null;
            this.batch.dispose();
            this.init = false;
        }
    }
}
