package com.chemproject.game.Screens.SreenTransition;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface IScreenTransition {
    float getDuration();

    void render(SpriteBatch spriteBatch, Texture texture, Texture texture2, float f);
}
