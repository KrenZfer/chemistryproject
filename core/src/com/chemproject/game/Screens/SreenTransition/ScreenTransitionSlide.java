package com.chemproject.game.Screens.SreenTransition;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;

public class ScreenTransitionSlide implements IScreenTransition {
    public static final int DOWN = 4;
    public static final int LEFT = 1;
    public static final int RIGHT = 2;
    public static final int UP = 3;
    private static final ScreenTransitionSlide instance = new ScreenTransitionSlide();
    private int direction;
    private float duration;
    private Interpolation easing;
    private boolean slideout;

    public static ScreenTransitionSlide init(float duration, int direction, boolean slideout, Interpolation easing) {
        instance.duration = duration;
        instance.direction = direction;
        instance.slideout = slideout;
        instance.easing = easing;
        return instance;
    }

    public float getDuration() {
        return this.duration;
    }

    public void render(SpriteBatch batch, Texture currScreen, Texture nextScreen, float alpha) {
        Texture texBottom;
        Texture texTop;
        float w = (float) currScreen.getWidth();
        float h = (float) currScreen.getHeight();
        float x = 0.0f;
        float y = 0.0f;
        if (this.easing != null) {
            alpha = this.easing.apply(alpha);
        }
        switch (this.direction) {
            case 1:
                x = (-w) * alpha;
                if (!this.slideout) {
                    x += w;
                    break;
                }
                break;
            case 2:
                x = w * alpha;
                if (!this.slideout) {
                    x -= w;
                    break;
                }
                break;
            case 3:
                y = h * alpha;
                if (!this.slideout) {
                    y -= h;
                    break;
                }
                break;
            case 4:
                y = (-h) * alpha;
                if (!this.slideout) {
                    y += h;
                    break;
                }
                break;
        }
        if (this.slideout) {
            texBottom = nextScreen;
        } else {
            texBottom = currScreen;
        }
        if (this.slideout) {
            texTop = currScreen;
        } else {
            texTop = nextScreen;
        }
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(texBottom, 0.0f, 0.0f, 0.0f, 0.0f, w, h, 1.0f, 1.0f, 0.0f, 0, 0, currScreen.getWidth(), currScreen.getHeight(), false, true);
        batch.draw(texTop, x, y, 0.0f, 0.0f, w, h, 1.0f, 1.0f, 0.0f, 0, 0, nextScreen.getWidth(), nextScreen.getHeight(), false, true);
        batch.end();
    }
}
