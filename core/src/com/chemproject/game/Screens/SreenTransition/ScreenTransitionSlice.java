package com.chemproject.game.Screens.SreenTransition;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.Array;

public class ScreenTransitionSlice implements IScreenTransition {
    public static final int DOWN = 2;
    public static final int UP = 1;
    public static final int UP_DOWN = 3;
    private static final ScreenTransitionSlice instance = new ScreenTransitionSlice();
    private int direction;
    private float duration;
    private Interpolation easing;
    private Array<Integer> sliceIndex = new Array();

    public static ScreenTransitionSlice init(float duration, int direction, int numSlices, Interpolation easing) {
        instance.duration = duration;
        instance.direction = direction;
        instance.easing = easing;
        instance.sliceIndex.clear();
        for (int i = 0; i < numSlices; i++) {
            instance.sliceIndex.add(Integer.valueOf(i));
        }
        instance.sliceIndex.shuffle();
        return instance;
    }

    public float getDuration() {
        return this.duration;
    }

    public void render(SpriteBatch batch, Texture currScreen, Texture nextScreen, float alpha) {
        float w = (float) currScreen.getWidth();
        float h = (float) currScreen.getHeight();
        int sliceWidth = (int) (w / ((float) this.sliceIndex.size));
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        batch.draw(currScreen, 0.0f, 0.0f, 0.0f, 0.0f, w, h, 1.0f, 1.0f, 0.0f, 0, 0, currScreen.getWidth(), currScreen.getHeight(), false, true);
        if (this.easing != null) {
            alpha = this.easing.apply(alpha);
        }
        float y = 0.0f;
        float x = 0.0f;
        for (int i = 0; i < this.sliceIndex.size; i++) {
            x = (float) (i * sliceWidth);
            float offsetY = h * ((((float) ((Integer) this.sliceIndex.get(i)).intValue()) / ((float) this.sliceIndex.size)) + 1.0f);
            switch (this.direction) {
                case 1:
                    y = (-offsetY) + (offsetY * alpha);
                    break;
                case 2:
                    y = offsetY - (offsetY * alpha);
                    break;
                case 3:
                    if (i % 2 != 0) {
                        y = offsetY - (offsetY * alpha);
                        break;
                    } else {
                        y = (-offsetY) + (offsetY * alpha);
                        break;
                    }
                default:
                    break;
            }
            batch.draw(nextScreen, x, y, 0.0f, 0.0f, (float) sliceWidth, h, 1.0f, 1.0f, 0.0f, i * sliceWidth, 0, sliceWidth, nextScreen.getHeight(), false, true);
        }
        batch.end();
    }
}
