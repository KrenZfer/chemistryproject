package com.chemproject.game.Screens;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.GamePreferences;
import com.chemproject.game.WorldController;
import com.chemproject.game.WorldRenderer;

public class GameScreen extends AbstractGameScreen {
    private static final String TAG = GameScreen.class.getName();
    private boolean paused;
    private WorldController worldController;
    private WorldRenderer worldRenderer;

    public GameScreen(DirectedGame game) {
        super(game);
    }

    @Override
    public void render(float deltaTime) {
        if (!this.paused && !worldRenderer.getUiGame().onPause) {
            this.worldController.update(deltaTime);
        }
        float height = this.worldController.cameraHelper.getPosition().y;
        float green = (150.0f - (1.0f - (((32.0f - height) / 32.0f) * 150.0f))) / 255.0f;
        float blue = (255.0f - (1.0f - (((32.0f - height) / 32.0f) * 255.0f))) / 255.0f;
        Gdx.gl20.glClearColor(0.7921569f, 0.89411765f, 0.7490196f, 1.0f);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT);
        this.worldRenderer.render();
    }

    @Override
    public void resize(int width, int height) {
        this.worldRenderer.resize(width, height);
    }

    @Override
    public void show() {
        GamePreferences.instance.load();
        this.worldController = new WorldController(this.game);
        this.worldRenderer = new WorldRenderer(this.worldController);
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void hide() {
        this.worldRenderer.dispose();
        Gdx.input.setCatchBackKey(false);
    }

    @Override
    public void pause() {
        this.paused = true;
    }

    @Override
    public InputProcessor getInputProcessor() {
        if (this.worldController.conver.isConversationDone) {
            return worldRenderer.getUiGame().getInputProcessor();
        }
        return this.worldController.conver.getInputProcessor();
    }

    @Override
    public void resume() {
        super.resume();
        this.paused = false;
    }
}
