package com.chemproject.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.Screens.SreenTransition.ScreenTransitionFade;
import com.chemproject.game.Screens.SreenTransition.ScreenTransitionSlice;
import com.chemproject.game.Screens.SreenTransition.ScreenTransitionSlide;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.AudioManager;
import com.chemproject.game.Utils.CharacterSkin;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.Constants.PreferenceConstants;
import com.chemproject.game.Utils.GamePreferences;

public class MenuScreen extends AbstractGameScreen {
    public static final String TAG = MenuScreen.class.getName();
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private Button btnMenuOptions;
    private Button btnMenuPlay;
    private Button btnMenuClose;
    private TextButton btnWinOptCancel;
    private TextButton btnWinOptSave;
    private CheckBox chkMusic;
    private CheckBox chkShowFpsCounter;
    private CheckBox chkSound;
    private boolean debugEnabled = false;
    private float debugRebuildStage;
    private Image imgBackground;
    private Image imgCharSkin;
    private SelectBox<CharacterSkin> selCharSkin;
    private Skin skinCanyonBunny;
    private Skin skinLibGdx;
    private Slider sldMusic;
    private Slider sldSound;
    private Stage stage;
    private Window winOptions;

    public MenuScreen(DirectedGame game) {
        super(game);
    }

    private void rebuildStage() {
        this.skinCanyonBunny = new Skin(Gdx.files.internal(Constants.SKIN_GAME_UI), new TextureAtlas(Constants.TEXTURE_ATLAS_UI));
        this.skinLibGdx = Assets.instance.skinLibGdx;
        Table layerBackground = buildBackgroundLayer();
        Table layerControls = buildControlsLayer();
        Table layerOptionsWindow = buildOptionsWindowLayer();
        Table layerTitleGame = buildTitleGame();
        this.stage.clear();
        Stack stack = new Stack();
        this.stage.addActor(stack);
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        stack.add(layerBackground);
        stack.add(layerControls);
        stack.add(layerOptionsWindow);
        stack.add(layerTitleGame);
    }

    private Table buildBackgroundLayer() {
        Table layer = new Table();
        this.imgBackground = new Image(this.skinCanyonBunny, "background");
        layer.background(this.imgBackground.getDrawable());
        return layer;
    }

    private Table buildTitleGame() {
        Table table = new Table();
        Label lbl = new Label("GAME EDUKASI LARUTAN PENYANGGA", new LabelStyle(Assets.instance.fonts.defaultBig, Color.MAROON));
        lbl.setFontScale(3.0f);
        table.left().top().add(lbl).pad(20.0f, 20.0f, 0.0f, 0.0f);
        return table;
    }

    private Table buildControlsLayer() {
        Table layer = new Table();
        layer.right().bottom();
        this.btnMenuPlay = new Button(this.skinCanyonBunny, "play");
        layer.add(this.btnMenuPlay).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(150.0f));
        this.btnMenuPlay.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                MenuScreen.this.onPlayClicked();
            }
        });
        layer.row();
        this.btnMenuOptions = new Button(this.skinCanyonBunny, "options");
        layer.add(this.btnMenuOptions).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(150.0f));
        this.btnMenuOptions.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                MenuScreen.this.onOptionsClicked();
            }
        });
        layer.row();
        this.btnMenuClose = new Button(this.skinCanyonBunny, "close");
        layer.add(this.btnMenuClose).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(150.0f));
        this.btnMenuClose.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                MenuScreen.this.onCloseClicked();
            }
        });
        if (this.debugEnabled) {
            layer.debug();
        }
        return layer;
    }

    private Table buildOptWinAudioSettings() {
        Table tbl = new Table();
        tbl.pad(10.0f, 10.0f, 0.0f, 10.0f);
        tbl.add(new Label(PreferenceConstants.AUDIO_SETTINGS_NAME, this.skinLibGdx, "default-font", Color.ORANGE)).colspan(3);
        tbl.row();
        tbl.columnDefaults(0).padRight(10.0f);
        tbl.columnDefaults(1).padRight(10.0f);
        this.chkSound = new CheckBox("", this.skinLibGdx);
        tbl.add(this.chkSound);
        tbl.add(new Label(PreferenceConstants.SOUND_SETTINGS_NAME, this.skinLibGdx));
        this.sldSound = new Slider(0.0f, 1.0f, 0.1f, false, this.skinLibGdx);
        tbl.add(this.sldSound);
        tbl.row();
        this.chkMusic = new CheckBox("", this.skinLibGdx);
        tbl.add(this.chkMusic);
        tbl.add(new Label(PreferenceConstants.MUSIC_SETTINGS_NAME, this.skinLibGdx));
        this.sldMusic = new Slider(0.0f, 1.0f, 0.1f, false, this.skinLibGdx);
        tbl.add(this.sldMusic);
        tbl.row();
        return tbl;
    }

    private Table buildOptWinSkinSelection() {
        Table tbl = new Table();
        tbl.pad(10.0f, 10.0f, 0.0f, 10.0f);
        tbl.add(new Label(PreferenceConstants.CHARSKIN_SETTINGS_NAME, this.skinLibGdx, "default-font", Color.ORANGE)).colspan(2);
        tbl.row();
        this.selCharSkin = new SelectBox(this.skinLibGdx);
        this.selCharSkin.setItems(CharacterSkin.values());
        this.selCharSkin.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                MenuScreen.this.onCharSkinSelected(((SelectBox) actor).getSelectedIndex());
            }
        });
        tbl.add(this.selCharSkin).width(120.0f).padRight(20.0f);
        this.imgCharSkin = new Image(Assets.instance.bunny.head);
        tbl.add(this.imgCharSkin).width(50.0f).height(50.0f);
        return tbl;
    }

    private Table buildOptWinDebug() {
        Table tbl = new Table();
        tbl.pad(10.0f, 10.0f, 0.0f, 10.0f);
        tbl.add(new Label((CharSequence) "Debug", this.skinLibGdx, "default-font", Color.RED)).colspan(3);
        tbl.row();
        tbl.columnDefaults(0).padRight(10.0f);
        tbl.columnDefaults(1).padRight(10.0f);
        this.chkShowFpsCounter = new CheckBox("", this.skinLibGdx);
        tbl.add(new Label((CharSequence) "Show FPS Counter", this.skinLibGdx));
        tbl.add(this.chkShowFpsCounter);
        tbl.row();
        return tbl;
    }

    private Table buildOptWinButtons() {
        Table tbl = new Table();
        Label lbl = new Label("", this.skinLibGdx);
        lbl.setColor(0.75f, 0.75f, 0.75f, 1.0f);
        lbl.setStyle(new LabelStyle(lbl.getStyle()));
        lbl.getStyle().background = this.skinLibGdx.newDrawable("white");
        tbl.add(lbl).colspan(2).height(1.0f).width(220.0f).pad(0.0f, 0.0f, 0.0f, 1.0f);
        tbl.row();
        lbl = new Label("", this.skinLibGdx);
        lbl.setColor(0.5f, 0.5f, 0.5f, 1.0f);
        lbl.setStyle(new LabelStyle(lbl.getStyle()));
        lbl.getStyle().background = this.skinLibGdx.newDrawable("white");
        tbl.add(lbl).colspan(2).height(1.0f).width(220.0f).pad(0.0f, 1.0f, 5.0f, 0.0f);
        tbl.row();
        this.btnWinOptSave = new TextButton(PreferenceConstants.SAVE_BUTTON_NAME, this.skinLibGdx);
        tbl.add(this.btnWinOptSave).padRight(30.0f);
        this.btnWinOptSave.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                MenuScreen.this.onSaveClicked();
            }
        });
        this.btnWinOptCancel = new TextButton(PreferenceConstants.CANCEL_BUTTON_NAME, this.skinLibGdx);
        tbl.add(this.btnWinOptCancel);
        this.btnWinOptCancel.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                MenuScreen.this.onCancelClicked();
            }
        });
        return tbl;
    }

    private void onOptionsClicked() {
        loadSettings();
        this.btnMenuPlay.setVisible(false);
        this.btnMenuOptions.setVisible(false);
        this.winOptions.setVisible(true);
    }

    private void loadSettings() {
        GamePreferences preferences = GamePreferences.instance;
        preferences.load();
        this.chkSound.setChecked(preferences.sound);
        this.sldMusic.setValue(preferences.volSound);
        this.chkMusic.setChecked(preferences.music);
        this.sldMusic.setValue(preferences.volMusic);
        this.selCharSkin.setSelectedIndex(preferences.charSkin);
        onCharSkinSelected(preferences.charSkin);
        this.chkShowFpsCounter.setChecked(preferences.showFpsCounter);
    }

    private void saveSettings() {
        GamePreferences preferences = GamePreferences.instance;
        preferences.sound = this.chkSound.isChecked();
        preferences.volSound = this.sldSound.getValue();
        preferences.music = this.chkMusic.isChecked();
        preferences.volMusic = this.sldMusic.getValue();
        preferences.charSkin = this.selCharSkin.getSelectedIndex();
        preferences.showFpsCounter = this.chkShowFpsCounter.isChecked();
        preferences.save();
    }

    private void onCharSkinSelected(int charSkin) {
        this.imgCharSkin.setColor(CharacterSkin.values()[charSkin].getColor());
    }

    private void onSaveClicked() {
        saveSettings();
        onCancelClicked();
        AudioManager.instance.onSettingUpdated();
    }

    private void onCancelClicked() {
        this.btnMenuPlay.setVisible(true);
        this.btnMenuOptions.setVisible(true);
        this.winOptions.setVisible(false);
        AudioManager.instance.onSettingUpdated();
    }

    private void onPlayClicked() {
        ScreenTransitionFade transitionFade = ScreenTransitionFade.init(0.75f);
        ScreenTransitionSlide transitionSlide = ScreenTransitionSlide.init(0.75f, 4, false, Interpolation.pow5Out);
        ScreenTransitionSlice transitionSlice = ScreenTransitionSlice.init(2.0f, 1, 10, Interpolation.pow5Out);
        this.game.setScreen(new GameScreen(this.game), transitionSlide);
    }

    private Table buildOptionsWindowLayer() {
        this.winOptions = new Window(PreferenceConstants.OPTION_WINDOW_NAME, this.skinLibGdx);
        this.winOptions.add(buildOptWinAudioSettings()).row();
        this.winOptions.add(buildOptWinSkinSelection()).row();
        this.winOptions.add(buildOptWinDebug()).row();
        this.winOptions.add(buildOptWinButtons()).pad(10.0f, 0.0f, 10.0f, 10.0f);
        this.winOptions.setColor(1.0f, 1.0f, 1.0f, 0.8f);
        this.winOptions.setVisible(false);
        if (this.debugEnabled) {
            this.winOptions.debug();
        }
        this.winOptions.pack();
        this.winOptions.setPosition((Constants.VIEWPORT_GUI_WIDTH - this.winOptions.getWidth()) - 50.0f, 50.0f);
        return this.winOptions;
    }


    private void onCloseClicked() {
        Gdx.app.exit();
    }

    public void render(float deltaTime) {
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (this.debugEnabled) {
            this.debugRebuildStage -= deltaTime;
            if (this.debugRebuildStage <= 0.0f) {
                this.debugRebuildStage = 5.0f;
                rebuildStage();
            }
        }
        this.stage.act(deltaTime);
        this.stage.draw();
        this.stage.setDebugTableUnderMouse(this.debugEnabled);
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height, true);
    }

    public void show() {
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        rebuildStage();
    }

    public void hide() {
        this.stage.dispose();
        this.skinCanyonBunny.dispose();
        this.skinLibGdx.dispose();
    }

    public void pause() {
    }

    public InputProcessor getInputProcessor() {
        return this.stage;
    }
}
