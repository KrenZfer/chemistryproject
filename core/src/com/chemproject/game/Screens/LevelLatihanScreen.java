package com.chemproject.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.ChemistryProject;
import com.chemproject.game.Model.LevelSoalLatihan;
import com.chemproject.game.Model.LevelSoalLatihan.PerLevelSoal;
import com.chemproject.game.Model.SoalLatihanData;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.TextRenderer;

import java.util.Iterator;

public class LevelLatihanScreen extends AbstractGameScreen {
    public static final String TAG = Level3Screen.class.getName();
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private final int TIME_COUNTDOWN = 50;
    private Array<Actor> actJawaban;
    private Image background;
    private Table backgroundTable;
    private Stack bookStack;
    private TextButton btnJawab;
    private ChangeListener change;
    private SPECIAL_ACTOR currentActor = SPECIAL_ACTOR.NONE;
    private Array<SoalLatihanData> currentLevel;
    private SoalLatihanData currentSoal;
    private boolean debugEnabled = false;
    private float debugRebuildStage;
    private boolean decisionAnswer = false;
    private Table firstPage;
    private Label judulSoal;
    private Label lblJawaban;
    private int level;
    private AbstractGameScreen nextScreen;
    private Image openBook;
    private ButtonGroup<CheckBox> pilihan;
    private float pointTotal = 0.0f;
    private Table secondPage;
    private int soal = 1;
    private LevelSoalLatihan soalLatihan;
    private float soalTotal = 0.0f;
    private Stage stage;
    private Table tableContent;

    private enum SPECIAL_ACTOR {
        IMG,
        FORMULA,
        NONE
    }

    public LevelLatihanScreen(DirectedGame game, int level, AbstractGameScreen nextScreen) {
        super(game);
        this.level = level;
        this.nextScreen = nextScreen;
    }

    private void initLevel() {
        this.soalLatihan = (LevelSoalLatihan) new Json().fromJson(LevelSoalLatihan.class, Gdx.files.internal("Data/SoalLatihanData.json"));
        this.soalLatihan.init();
        this.currentLevel = ((PerLevelSoal) this.soalLatihan.getSoals().get(Integer.valueOf(this.level))).getSoalsoal();
        this.currentSoal = (SoalLatihanData) this.currentLevel.get(this.soal - 1);
    }

    public void render(float deltaTime) {
        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (this.debugEnabled) {
            this.debugRebuildStage -= deltaTime;
            if (this.debugRebuildStage <= 0.0f) {
                this.debugRebuildStage = 5.0f;
                rebuildStage();
            }
        }
        this.stage.act(deltaTime);
        this.stage.draw();
        this.stage.setDebugTableUnderMouse(this.debugEnabled);
    }

    private void rebuildStage() {
        this.backgroundTable = buildTableBackground();
        this.bookStack = buildBukuSoal();
        Stack stack = new Stack();
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        stack.add(this.backgroundTable);
        stack.add(this.bookStack);
        this.stage.addActor(stack);
    }

    private Table buildTableBackground() {
        Table table = new Table();
        this.background = new Image(Assets.instance.assetBook.background);
        table.background(this.background.getDrawable());
        return table;
    }

    private Stack buildBukuSoal() {
        Stack stack = new Stack();
        Table tableBook = new Table();
        this.tableContent = new Table();
        LabelStyle bigStyle = new LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK);
        this.btnJawab = new TextButton("Jawab", Assets.instance.skinLibGdx);
        this.judulSoal = new Label("", bigStyle);
        this.pilihan = new ButtonGroup();
        this.openBook = new Image(Assets.instance.assetBook.openBook);
        this.firstPage = new Table();
        this.secondPage = new Table();
        tableBook.background(this.openBook.getDrawable());
        this.change = new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                LevelLatihanScreen.this.checkJawaban();
            }
        };
        this.btnJawab.addListener(this.change);
        this.firstPage.setSize(Constants.getRealWidth(500.0f), Constants.getRealHeight(500.0f));
        this.secondPage.setSize(Constants.getRealWidth(500.0f), Constants.getRealHeight(500.0f));
        this.secondPage.setTouchable(Touchable.childrenOnly);
        this.firstPage.add(buildSpecial(this.currentSoal.getTextSoal())).size(Constants.getRealWidth(500.0f), Constants.getRealHeight(250.0f));
        this.actJawaban = buildJawaban(this.currentSoal.getJawabanSoal());
        Iterator it = this.actJawaban.iterator();
        while (it.hasNext()) {
            Actor act = (Actor) it.next();
            CheckBox temp = new CheckBox("", Assets.instance.skinLibGdx);
            temp.getImage().setScaling(Scaling.fill);
            temp.getImageCell().size(Constants.getRealWidth(30.0f), Constants.getRealHeight(30.0f));
            this.pilihan.add(temp);
            this.secondPage.add(temp).pad(0.0f, 0.0f, Constants.getRealHeight(10.0f), Constants.getRealWidth(10.0f));
            this.secondPage.add(act).width(Constants.getRealWidth(400.0f)).align(8).padBottom(Constants.getRealHeight(10.0f));
            this.secondPage.row();
        }
        this.pilihan.uncheckAll();
        stack.setSize(this.openBook.getImageWidth(), this.openBook.getImageHeight());
        this.judulSoal.setText("Level " + this.level + " : Soal " + this.soal);
        this.tableContent.add(this.judulSoal).padBottom(Constants.getRealHeight(20.0f)).align(1);
        this.tableContent.row();
        this.tableContent.left().add(this.firstPage).pad(0.0f, Constants.getRealWidth(60.0f), 0.0f, 0.0f);
        this.tableContent.add(this.secondPage).pad(0.0f, Constants.getRealWidth(160.0f), 0.0f, 0.0f).left();
        tableBook.bottom().right().add(this.btnJawab).size(Constants.getRealWidth(100.0f), Constants.getRealHeight(40.0f)).pad(0.0f, 0.0f, Constants.getRealHeight(50.0f), Constants.getRealWidth(60.0f));
        stack.add(tableBook);
        stack.add(this.tableContent);
        return stack;
    }

    private void checkJawaban() {
        if (this.pilihan.getCheckedIndex() != -1) {
            this.soalTotal += 1.0f;
            if (this.pilihan.getCheckedIndex() == this.currentSoal.getIndeksJawabanBenar() - 1) {
                this.pointTotal += 1.0f;
            }
            if (this.soal < this.currentLevel.size) {
                this.soal++;
                initLevel();
                rebuildStage();
                return;
            }
            tampilkanHasilAkhir();
        }
    }

    private void tampilkanHasilAkhir() {
        this.tableContent.clearChildren();
        Table table = new Table();
        CharSequence nilaiTulis = ((this.pointTotal / this.soalTotal) * 100.0f) + "%";
        Actor label = new Label((CharSequence) "NILAI", new LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK));
        Actor nilai = new Label(nilaiTulis, new LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK));
        table.add(label);
        table.row();
        table.add(nilai);
        this.tableContent.left().add(table).padLeft(Constants.getRealWidth(300.0f));
        this.btnJawab.setText("Selanjutnya");
        this.btnJawab.removeListener(this.change);
        this.btnJawab.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                LevelLatihanScreen.this.game.setScreen(LevelLatihanScreen.this.nextScreen, Constants.screenSlide);
            }
        });
    }

    private Table buildPemberitahuan(String text, Color color) {
        Table table = new Table();
        Actor label = new Label((CharSequence) text, new LabelStyle(Assets.instance.fonts.defaultBig, color));
        Pixmap pixmap = new Pixmap(1280, (int) Constants.SCREEN_HEIGHT, Format.RGBA8888);
        pixmap.setColor(0.5f, 0.5f, 0.5f, 0.5f);
        pixmap.fillRectangle(0, 0, (int) Constants.VIEWPORT_GUI_WIDTH, (int) Constants.VIEWPORT_GUI_HEIGHT);
        table.setBackground(new Image(new Texture(pixmap)).getDrawable());
        table.add(label);
        return table;
    }

    private Actor buildSpecial(String soal) {
        Actor act = new Actor();
        boolean special = false;
        boolean readSpecial = false;
        String temp = "";
        String word = "";
        this.currentActor = SPECIAL_ACTOR.NONE;
        java.util.Stack<String> specialWord = new java.util.Stack();
        for (int i = 0; i < soal.length(); i++) {
            if (soal.charAt(i) == '<') {
                if (!special) {
                    special = true;
                }
                if (readSpecial) {
                    word = temp;
                }
            } else if (soal.charAt(i) == '>') {
                special = false;
                if (temp.equalsIgnoreCase("img")) {
                    this.currentActor = SPECIAL_ACTOR.IMG;
                    readSpecial = true;
                } else if (temp.equalsIgnoreCase("formula")) {
                    this.currentActor = SPECIAL_ACTOR.FORMULA;
                    readSpecial = true;
                }
                if (specialWord.empty() || !(specialWord.firstElement()).equalsIgnoreCase(temp)) {
                    specialWord.push(temp);
                } else if ((specialWord.firstElement()).equalsIgnoreCase(temp)) {
                    specialWord.pop();
                }
                temp = "";
            } else {
                temp = temp + soal.charAt(i);
            }
        }
        if (this.currentActor == SPECIAL_ACTOR.NONE) {
            act = new Label(temp, new LabelStyle(Assets.instance.fonts.defaultMedium, Color.BLACK));
            ((Label) act).setWrap(true);
            return act;
        } else if (this.currentActor == SPECIAL_ACTOR.IMG) {
            act = new Image((TextureRegion) Assets.instance.soalLatihan.soal.get(word));
            ((Image) act).setScaling(Scaling.fit);
            return act;
        } else if (this.currentActor == SPECIAL_ACTOR.FORMULA) {
            return TextRenderer.renderString((ChemistryProject) this.game, Assets.instance.skinLibGdx, word, Color.BLACK);
        } else {
            return act;
        }
    }

    private Array<Actor> buildJawaban(Array<String> jawaban) {
        Array<Actor> jawab = new Array();
        Iterator it = jawaban.iterator();
        while (it.hasNext()) {
            Actor a = buildSpecial((String) it.next());
            a.addListener(new ChangeListener() {
                public void changed(ChangeEvent event, Actor actor) {
                    Gdx.app.debug(LevelLatihanScreen.TAG, "MASUKKKK!!!!!!!!");
                }
            });
            jawab.add(a);
        }
        return jawab;
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height);
    }

    public void show() {
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        initLevel();
        rebuildStage();
    }

    public void hide() {
        this.stage.dispose();
    }

    public void pause() {
    }

    public void resume() {
        super.resume();
    }

    public InputProcessor getInputProcessor() {
        return this.stage;
    }
}
