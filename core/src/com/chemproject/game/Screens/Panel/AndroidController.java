package com.chemproject.game.Screens.Panel;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.CameraHelper;
import com.chemproject.game.Utils.Constants;

public class AndroidController {
    boolean backKeyPressed;
    boolean jumpKeyPressed;
    boolean leftKeyPressed;
    boolean rightKeyPressed;
    private Stage stage;
    private Table tablePad = new Table();

    public AndroidController() {
    }

    public void init(OrthographicCamera cam, SpriteBatch batch){
        this.stage = new Stage(new StretchViewport(cam.viewportWidth, cam.viewportHeight), batch);
        this.tablePad.setFillParent(true);
        this.tablePad.setTouchable(Touchable.enabled);
        this.tablePad.bottom().left();
        Actor imgPadLeft = new Image(Assets.instance.pads.padLeft);
        imgPadLeft.setSize(Constants.getRealWidth(50.0f), Constants.getRealHeight(50.0f));
        imgPadLeft.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AndroidController.this.leftKeyPressed = true;
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                AndroidController.this.leftKeyPressed = false;
            }
        });
        Actor imgPadRight = new Image(Assets.instance.pads.padRight);
        imgPadRight.setSize(Constants.getRealWidth(50.0f), Constants.getRealHeight(50.0f));
        imgPadRight.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AndroidController.this.rightKeyPressed = true;
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                AndroidController.this.rightKeyPressed = false;
            }
        });
        Actor imgPadJump = new Image(Assets.instance.pads.padJump);
        imgPadJump.setSize(Constants.getRealWidth(50.0f), Constants.getRealHeight(50.0f));
        imgPadJump.addListener(new InputListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                AndroidController.this.jumpKeyPressed = true;
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                AndroidController.this.jumpKeyPressed = false;
            }
        });
        this.tablePad.add(imgPadLeft).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(150.0f)).pad(5.0f, 5.0f, 10.0f, 10.0f);
        this.tablePad.add(imgPadRight).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(150.0f)).pad(5.0f, 10.0f, 10.0f, 5.0f);
        this.tablePad.add(imgPadJump).size(Constants.getRealWidth(150.0f), Constants.getRealHeight(150.0f)).pad(5.0f, 5.0f, 10.0f, 5.0f).expandX().right();
        this.stage.addActor(this.tablePad);
//        this.stage.addListener(new InputListener() {
//            public boolean keyDown(InputEvent event, int keycode) {
//                if (keycode == 4) {
//                    AndroidController.this.backKeyPressed = true;
//                }
//                return true;
//            }
//
//            public boolean keyUp(InputEvent event, int keycode) {
//                if (keycode == 4) {
//                    AndroidController.this.backKeyPressed = false;
//                }
//                return false;
//            }
//        });
    }

    public void draw() {
        this.stage.act();
        this.stage.draw();
    }

    public void dispose() {
        this.stage.dispose();
    }

    public void resize(float width, float height) {
        this.stage.getViewport().update((int) width, (int) height, true);
    }

    public Stage getStage() {
        return this.stage;
    }

    public boolean isJumpKeyPressed() {
        return this.jumpKeyPressed;
    }

    public boolean isLeftKeyPressed() {
        return this.leftKeyPressed;
    }

    public boolean isRightKeyPressed() {
        return this.rightKeyPressed;
    }

    public boolean isBackKeyPressed() {
        return this.backKeyPressed;
    }
}
