package com.chemproject.game.Screens.Panel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;

public class ConversationPanel extends Stack {
    String alignPanel;
    Label conversationLabel;
    Table conversationTable;
    String conversationText;
    public float delayForNextDialog;
    float delta = 0.0f;
    String imageObject;
    Image imagePerson;
    public boolean isTextDoneWriting;
    Label labelClick;
    final float letterSpawntime = 0.3f;
    Label nameLabel;
    String namePerson;
    String pathImgPerson;
    int stringIndex = 0;
    final float timeDelay = 4.0f;
    float timer = 0.0f;
    String visualTemp = "";

    public ConversationPanel(String conversationText, String namePerson, String imageObject, String alignPanel, String pathImgPerson) {
        this.conversationText = conversationText;
        this.namePerson = namePerson;
        this.imageObject = imageObject;
        this.alignPanel = alignPanel;
        this.pathImgPerson = pathImgPerson;
        this.isTextDoneWriting = false;
        init();
    }

    private void init() {
        this.delayForNextDialog = 4.0f;
        this.conversationTable = buildConversationTable();
        setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        add(this.conversationTable);
    }

    private Table buildConversationTable() {
        Stack scene = new Stack();
        Table table = new Table();
        Table conversationTab = new Table();
        conversationTab.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        conversationTab.setFillParent(true);
        Table personTable = new Table();
        personTable.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.imagePerson = new Image((TextureRegion) Assets.instance.karakter.koleksiKarakter.get(this.pathImgPerson));
        this.conversationLabel = new Label("", new LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK));
        this.conversationLabel.setWrap(true);
        this.conversationLabel.setAlignment(10);
        this.nameLabel = new Label(this.namePerson, new LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK));
        this.labelClick = new Label((CharSequence) "SELANJUTNYA", new LabelStyle(Assets.instance.fonts.defaultNormal, Color.WHITE));
        Table tableName = new Table();
        tableName.background(new Image(Assets.instance.assetFormulaContent.pieceFrame).getDrawable());
        tableName.add(this.nameLabel);
        Table tableConverst = new Table();
        tableConverst.setTouchable(Touchable.enabled);
        tableConverst.addListener(new ClickListener() {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                ConversationPanel.this.clickToContinue();
                return true;
            }
        });
        tableConverst.background(new Image(Assets.instance.assetFormulaContent.pieceFrame).getDrawable());
        tableConverst.top().left();
        tableConverst.add(this.conversationLabel).size(Constants.getRealWidth(1150.0f), Constants.getRealHeight(130.0f)).pad(10.0f, 50.0f, 10.0f, 10.0f);
        tableConverst.row();
        tableConverst.add(this.labelClick).size(Constants.getRealWidth(30.0f), Constants.getRealHeight(20.0f)).align(16).pad(0.0f, 0.0f, 20.0f, 70.0f);
        conversationTab.bottom().left();
        if (this.alignPanel.equalsIgnoreCase("LEFT")) {
            conversationTab.add(tableName).size(Constants.getRealWidth(105.0f), Constants.getRealHeight(50.0f)).pad(0.0f, 10.0f, 0.0f, 10.0f).align(8);
            personTable.bottom().left().add(this.imagePerson).size(Constants.getRealWidth(300.0f), Constants.getRealHeight(600.0f));
        } else {
            conversationTab.add(tableName).size(Constants.getRealWidth(105.0f), Constants.getRealHeight(50.0f)).pad(0.0f, 10.0f, 0.0f, 10.0f).align(16);
            personTable.bottom().right().add(this.imagePerson).size(Constants.getRealWidth(300.0f), Constants.getRealHeight(600.0f));
        }
        conversationTab.row();
        conversationTab.add(tableConverst).pad(0.0f, 10.0f, 0.0f, 10.0f).size(Constants.VIEWPORT_GUI_WIDTH - Constants.getRealWidth(20.0f), Constants.getRealHeight(200.0f));
        scene.add(personTable);
        scene.add(conversationTab);
        table.bottom().left().add(scene);
        return table;
    }

    private void clickToContinue() {
        if (this.visualTemp.length() != this.conversationText.length()) {
            this.visualTemp = this.conversationText;
        } else {
            this.isTextDoneWriting = true;
        }
    }

    public void updateText() {
        this.delta = Gdx.graphics.getDeltaTime();
        if (this.isTextDoneWriting) {
            this.timer += this.delta;
        }
        if (this.visualTemp.length() < this.conversationText.length()) {
            this.labelClick.setVisible(false);
            if (this.timer <= 0.3f) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.visualTemp);
                sb.append(this.conversationText.charAt(this.stringIndex));
                this.visualTemp = sb.toString();
                this.stringIndex++;
                this.timer = 0.0f;
            }
        } else {
            this.labelClick.setVisible(true);
            this.delayForNextDialog -= this.delta;
//            if (this.delayForNextDialog < 0.0f) {
//                this.isTextDoneWriting = true;
//            }
        }
        this.conversationLabel.setText(this.visualTemp);
    }

    public void panelDone() {
        remove();
        this.delayForNextDialog = 4.0f;
        this.isTextDoneWriting = false;
        this.visualTemp = "";
        this.stringIndex = 0;
        this.timer = 0.0f;
        this.delta = 0.0f;
    }
}
