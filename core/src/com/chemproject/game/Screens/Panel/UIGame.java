package com.chemproject.game.Screens.Panel;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.GamePreferences;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class UIGame {
    private final Skin skinCanyonBunny;
    private Image chemSymbol;
    private Button btnPause;
    private Label finalScore;
    private Label fpsCounter;
    private Label gameOverMessage;
    private int lives;
    private Image[] livesImage;
    private Label score;
    private Stack stackUI = new Stack();
    private Stage stage;
    private Table tableFpsCounter;
    private Table tableGameOverMessage;
    private Table tableLives;
    private Table tableScore;
    private Table tablePause;
    private AndroidController androidController;
    public boolean onPause = false;

    private OrthographicCamera camera;
    private SpriteBatch batch;

    //ui when pause
    private Table tableUIPause;
    private Button btnLanjutkan;
    private Button btnBantuan;
    private Button btnMenuUtama;
    private Table UIMenuBantuan;
    public boolean toMenu = false;
    public int level;


    public UIGame(OrthographicCamera cam, SpriteBatch batch) {
//        new FitViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT)
        this.camera = cam;
        this.batch = batch;
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT), batch);
        this.skinCanyonBunny = new Skin(Gdx.files.internal(Constants.SKIN_GAME_UI), new TextureAtlas(Constants.TEXTURE_ATLAS_UI));
        androidController = new AndroidController();
    }

    public void init(int level) {
        this.level = level;
        this.stackUI.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        if (level == 1) {
            if (Gdx.app.getType() == Application.ApplicationType.Android)
                this.androidController.init(camera, batch);
//        this.stackUI.setTouchable(Touchable.enabled);
            this.tableScore = buildScoreGame();
            this.tableLives = buildLivesGame();
            this.tableFpsCounter = buildFPSCounter();
            this.tableGameOverMessage = buildGameOverMessage();
            this.stackUI.add(this.tableScore);
            this.stackUI.add(this.tableLives);
            this.stackUI.add(this.tableFpsCounter);
            this.stackUI.add(this.tableGameOverMessage);
        }
        this.tablePause = buildPauseBtn();
        this.tableUIPause = new Table();
        tableUIPause.add(buildUIPause());
        this.UIMenuBantuan = buildMenuBantuan(level);
        this.stackUI.add(this.tablePause);
        this.stage.addActor(this.stackUI);
    }

    private Stack buildUIPause(){
        Stack stack = new Stack();
        Table table = new Table();
        Table btnTable = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        stack.setTouchable(Touchable.enabled);
        btnTable.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        btnTable.setTouchable(Touchable.enabled);

        Image panel = new Image(Assets.instance.mig.panel_pause);
        btnLanjutkan = new Button(Assets.instance.mig.menuInGame, "lanjutkan");
        btnBantuan = new Button(Assets.instance.mig.menuInGame, "bantuan");
        btnMenuUtama = new Button(Assets.instance.mig.menuInGame, "menuutama");

        table.add(panel);
        btnTable.add(btnLanjutkan)
                .padTop(Constants.getRealHeight(50.0f));
        btnTable.row();
        btnTable.add(btnBantuan).padTop(Constants.getRealHeight(10.0f));
        btnTable.row();
        btnTable.add(btnMenuUtama).padTop(Constants.getRealHeight(10.0f));

        btnLanjutkan.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                btnLanjutkanClicked();
            }
        });

        btnMenuUtama.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                backtoMenu();
            }
        });

        btnBantuan.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                showMenuBantuan(true);
            }
        });

        stack.add(table);
        stack.add(btnTable);

        return stack;
    }

    private void showMenuBantuan(boolean b) {
        if (b){
            stackUI.add(UIMenuBantuan);
            tableUIPause.setTouchable(Touchable.disabled);
        }else {
            stackUI.removeActor(UIMenuBantuan);
            tableUIPause.setTouchable(Touchable.enabled);
        }
    }

    private void backtoMenu() {
        toMenu = true;
    }

    private void btnLanjutkanClicked() {
        onPause = false;
        stackUI.removeActor(tableUIPause);
        btnPause.setDisabled(onPause);
    }


    private void pauseBtnClicked() {
        onPause = true;
        stackUI.add(tableUIPause);
        btnPause.setDisabled(onPause);
    }

    private Table buildMenuBantuan(int level){
        Table main = new Table();
        Stack stack = new Stack();
        Table table = new Table();
        Table panduan = new Table();
        Array<Label> panduans = new Array<Label>();
        LabelStyle style = new LabelStyle(Assets.instance.fonts.defaultMedium, Color.WHITE);
        Array<String> teksPanduan = Assets.instance.panduan.panduan.getTeksArray().get(level-1).getTeks();
        for (String s : teksPanduan){
            panduans.add(new Label(s, style));
        }
        main.setSize(Constants.getRealWidth(800), Constants.getRealHeight(600));
        panduan.setSize(Constants.getRealWidth(800), Constants.getRealHeight(600));

        Image image = new Image(Assets.instance.mig.panel_bantuan);

        table.top().right();
        Button close = new Button(new Image(Assets.instance.assetBook.closeButton).getDrawable());
        close.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                showMenuBantuan(false);
            }
        });

        table.add(close).size(Constants.getRealWidth(50.0f), Constants.getRealHeight(50.0f));

        panduan.padTop(Constants.getRealHeight(60.0f));
        for (Label lbl : panduans){
            lbl.setWrap(true);
            panduan.left().add(lbl).size(Constants.getRealWidth(750), Constants.getRealHeight(50))
                    .pad(0.0f, Constants.getRealWidth(30.0f), Constants.getRealHeight(25.0f), 0.0f);
            panduan.row();
        }

        stack.add(image);
        stack.add(table);
        stack.add(panduan);

        main.add(stack).size(Constants.getRealWidth(800), Constants.getRealHeight(600));

        return main;
    }

    private Table buildPauseBtn(){
        Table table = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        table.setTouchable(Touchable.enabled);
        this.btnPause = new Button(skinCanyonBunny, "pause");
        table.top().right();
        table.add(btnPause).size(Constants.getRealWidth(100.0f), Constants.getRealHeight(100.0f));
        btnPause.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                pauseBtnClicked();
            }
        });
        return table;
    }

    private Table buildScoreGame() {
        Table table = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.score = new Label((CharSequence) "0/10", new LabelStyle(Assets.instance.fonts.defaultBig, Color.WHITE));
        this.chemSymbol = new Image(Assets.instance.levelDecoration.chemSymbol);
        table.top().left();
        table.add(this.chemSymbol).size(Constants.getRealWidth(70.0f), Constants.getRealHeight(70.0f)).pad(0.0f, 5.0f, 5.0f, 5.0f);
        table.add(this.score).size(Constants.getRealWidth(70.0f), Constants.getRealHeight(70.0f)).pad(0.0f, 5.0f, 5.0f, 5.0f);
        return table;
    }

    private Table buildLivesGame() {
        Table table = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.livesImage = new Image[3];
        table.top().left().padTop(Constants.getRealHeight(80.0f));
        for (int i = 0; i < this.livesImage.length; i++) {
            this.livesImage[i] = new Image(Assets.instance.bunny.heart);
            table.add(this.livesImage[i]).size(Constants.getRealWidth(40.0f), Constants.getRealHeight(40.0f)).pad(5.0f, 5.0f, 5.0f, 5.0f);
        }
        return table;
    }

    private Table buildFPSCounter() {
        Table table = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.fpsCounter = new Label("", new LabelStyle(Assets.instance.fonts.defaultNormal, Color.GREEN));
        table.center().top();
        table.add(this.fpsCounter).pad(5.0f, 5.0f, 5.0f, 5.0f);
        return table;
    }

    private Table buildGameOverMessage() {
        Table table = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.gameOverMessage = new Label("", new LabelStyle(Assets.instance.fonts.defaultBig, Color.WHITE));
        this.finalScore = new Label("", new LabelStyle(Assets.instance.fonts.defaultBig, Color.WHITE));
        table.center();
        table.add(this.gameOverMessage);
        table.row();
        table.add(this.finalScore);
        return table;
    }

    public void draw() {
        stage.act();
        this.stage.draw();
        if (Gdx.app.getType() == Application.ApplicationType.Android && level == 1)
            androidController.draw();
    }

    public void dispose() {
        this.stage.dispose();
        if (Gdx.app.getType() == Application.ApplicationType.Android && level == 1)
            androidController.dispose();
    }

    public void resize(float width, float height) {
        this.stage.getViewport().update((int) width, (int) height);
        if (Gdx.app.getType() == Application.ApplicationType.Android && level == 1)
            androidController.resize(width, height);
    }

    public void updateScore(int i) {
        this.score.setText("" + i + "/" + Assets.instance.assetFormulaContent.larutanPenyangga.size);
    }

    public void updateLives(int lives) {
        this.lives = lives;
        for (int i = 0; i < this.livesImage.length; i++) {
            if (i + 1 > lives) {
                this.livesImage[i].setColor(0.5f, 0.5f, 0.5f, 0.5f);
            }
        }
    }

    public void updateFpsCounter() {
        this.fpsCounter.setVisible(GamePreferences.instance.showFpsCounter);
        int fps = Gdx.graphics.getFramesPerSecond();
        if (fps >= 45) {
            this.fpsCounter.setColor(0.0f, 1.0f, 0.0f, 1.0f);
        } else if (fps >= 30) {
            this.fpsCounter.setColor(1.0f, 1.0f, 0.0f, 1.0f);
        } else {
            this.fpsCounter.setColor(1.0f, 0.0f, 0.0f, 1.0f);
        }
        this.fpsCounter.setText("FPS: " + fps);
    }

    public AndroidController getAndroidController() {
        return androidController;
    }

    public InputProcessor getInputProcessor(){
        InputMultiplexer multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);
        if (Gdx.app.getType() == Application.ApplicationType.Android && level == 1)
            multiplexer.addProcessor(androidController.getStage());
        return multiplexer;
    }

    public void gameWin(boolean isWin) {

        if (isWin) {
            this.gameOverMessage.setColor(0.0f, 1.0f, 0.0f, 1.0f);
            this.finalScore.setColor(0.0f, 1.0f, 0.0f, 1.0f);
            this.gameOverMessage.setText("YOU WIN!!!!");
        } else {
            this.gameOverMessage.setColor(1.0f, 0.0f, 0.0f, 1.0f);
            this.finalScore.setColor(1.0f, 0.0f, 0.0f, 1.0f);
            this.gameOverMessage.setText("YOU LOSE!!!!");
        }
        Constants.finalScoreLevel1 = (((float) this.lives) / 3.0f) * 100.0f;
        this.finalScore.setText(new DecimalFormat("##.##").format((double) Constants.finalScoreLevel1) + "%");
    }
}
