package com.chemproject.game.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;

public class ProfilScreen extends AbstractGameScreen {
    public static final String TAG = Level3Screen.class.getName();
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private final int TIME_COUNTDOWN = 50;
    private boolean debugEnabled = false;
    private float debugRebuildStage;
    private boolean isGameOver = false;
    private Stage stage;
    private Label.LabelStyle style;
    private Table tableContent;
    private Label judulSoal;
    private Image openBook;
    private Table firstPage, secondPage;
    private Stack profilTable;
    private Image background;


    public ProfilScreen(DirectedGame game) {
        super(game);
    }

    @Override
    public void render(float deltaTime) {
        Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (this.debugEnabled) {
            this.debugRebuildStage -= deltaTime;
            if (this.debugRebuildStage <= 0.0f) {
                this.debugRebuildStage = 5.0f;
                rebuildStage();
            }
        }
        this.stage.act(deltaTime);
        this.stage.draw();
        this.stage.setDebugAll(this.debugEnabled);
    }

    private void rebuildStage() {
//        this.profilTable = buildHalamanProfil();
        Stack bgTable = buildTableBackground();
//        stack.add(profilTable);
        stage.addActor(bgTable);
    }

    private void backToHome() {
        this.game.setScreen(new MenuScreen(this.game), Constants.screenSlide);
    }

    private Stack buildTableBackground() {
        Table table = new Table();
        Table uiTable = new Table();
        Button closeBtn = new Button(new Image(Assets.instance.assetBook.closeButton).getDrawable());
        closeBtn.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                backToHome();
            }
        });
        Stack stack = new Stack();
        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.background = new Image(Assets.instance.assetBook.background);
        table.background(this.background.getDrawable());
        Label.LabelStyle style = new Label.LabelStyle(Assets.instance.fonts.defaultMedium, Color.WHITE);
        Label judul = new Label("PROFIL PENGEMBANG", style);
        Label text1 = new Label("Media pembelajaran ini dikembangkan oleh:", style);
        Label text2 = new Label("Kikie Trivia Amalia", style);
        Label text3 = new Label("NIM: 140331605094", style);
        Label text4 = new Label("Program Studi S1 Pendidikan Kimia Universitas Negeri Malang", style);
        Image fotoprofil = new Image(Assets.instance.karakter.fotoProfil);
        table.add(judul).align(Align.left);
        table.row();
        table.add(text1).align(Align.left);
        table.row();
        table.add(fotoprofil).size(Constants.getRealWidth(100.0f), Constants.getRealHeight(150.0f)).pad(Constants.getRealHeight(10.0f),0.0f,Constants.getRealHeight(10.0f),0.0f);
        table.row();
        table.add(text2);
        table.row();
        table.add(text3);
        table.row();
        table.add(text4).padBottom(Constants.getRealHeight(190.0f));
        uiTable.top().right().add(closeBtn).size(Constants.getRealWidth(80.0f), Constants.getRealHeight(80.0f))
                .pad(Constants.getRealHeight(20.0f), 0.0f, 0.0f, Constants.getRealWidth(20.0f));
        stack.add(table);
        stack.add(uiTable);
        return stack;
    }

    private Stack buildHalamanProfil(){
        Stack stack = new Stack();
        Table tableBook = new Table();
        this.tableContent = new Table();
        Label.LabelStyle bigStyle = new Label.LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK);

        this.judulSoal = new Label("", bigStyle);
        this.openBook = new Image(Assets.instance.assetBook.openBook);
        this.firstPage = new Table();
        this.secondPage = new Table();
        tableBook.background(this.openBook.getDrawable());

        this.firstPage.setSize(Constants.getRealWidth(500.0f), Constants.getRealHeight(500.0f));
        this.secondPage.setSize(Constants.getRealWidth(500.0f), Constants.getRealHeight(500.0f));
        this.secondPage.setTouchable(Touchable.childrenOnly);

        stack.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.judulSoal.setText("Profil Pengembang");
        this.firstPage.top().center().add(this.judulSoal).align(Align.center);
//                .padBottom(Constants.getRealHeight(20.0f)).align(Align.top | Align.center);
        this.tableContent.row();
        this.tableContent.left().add(this.firstPage).pad(0.0f, Constants.getRealWidth(60.0f), 0.0f, 0.0f);
        this.tableContent.add(this.secondPage).pad(0.0f, Constants.getRealWidth(160.0f), 0.0f, 0.0f).left();
        stack.add(tableBook);
        stack.add(this.tableContent);
        return stack;
    }


    private void update(float deltaTime) {
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height);
    }

    public void show() {
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        rebuildStage();
    }

    public void hide() {
        this.stage.dispose();
    }

    public void pause() {
    }

    public InputProcessor getInputProcessor() {
        return this.stage;
    }

}
