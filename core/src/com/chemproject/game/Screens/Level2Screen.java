package com.chemproject.game.Screens;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.chemproject.game.Model.PairPiece;
import com.chemproject.game.Model.PuzzlePenyangga;
import com.chemproject.game.Screens.Panel.UIGame;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.ConversationPanelExec;

import java.text.DecimalFormat;

public class Level2Screen extends AbstractGameScreen {
    public static final String TAG = Level2Screen.class.getName();
    private final float DEBUG_REBUILD_INTERVAL = 5.0f;
    private final int TIME_COUNTDOWN = 60;
    private ConversationPanelExec conver = new ConversationPanelExec();
    private int counterAnswerTrue = 0;
    private boolean debugEnabled = false;
    private float debugRebuildStage;
    private Label decision;
    private Decision finalDecision = Decision.PLAY;
    private Label finalScore;
    private InfoPiece firstInfo;
    private boolean paused;
    private int penaltyScore = 0;
    private PuzzlePenyangga penyanggaList = ((PuzzlePenyangga) new Json().fromJson(PuzzlePenyangga.class, Gdx.files.internal(Constants.DATA_SOLUTION_JSON)));
    private Table pieceInformation;
    private float pieceInformationHeight = Constants.getRealHeight(720.0f);
    private float pieceInformationWidth = Constants.getRealWidth(380.0f);
    private String[] puzzle = new String[]{"1a", "2b", "3b", "7a", "4b", "2a", "1b", "5b", "6a", "7b", "4a", "6b", "8b", "5a", "3a", "8a"};
    private Table puzzleBoard;
    private float puzzleBoardHeight = Constants.getRealHeight(720.0f);
    private float puzzleBoardWidth = Constants.getRealWidth(900.0f);
    private Table puzzleGame;
    private InfoPiece secondInfo;
    private Stack stackMain;
    private Stage stage;
    private boolean startTimer = true;
    private Table theDecision;
    private int timeCountDown;
    private float timeleftForNextScreen;
    private Label timer;
    private Task timerTask;
    private UIGame uiGame;
    private SpriteBatch batch;
    private OrthographicCamera cameraGUI;

    enum Decision {
        WIN,
        LOSE,
        PLAY
    }

    class InfoPiece {
        public PiecePuzzle forInfo;
        public Table info;
        public boolean isFilled = false;

        public InfoPiece() {
            this.forInfo = new PiecePuzzle();
            makeInfoPiece();
        }

        public void makeInfoPiece() {
            this.info = new Table();
            this.info.background(new Image(Assets.instance.assetFormulaContent.pieceFrame).getDrawable());
        }

        public void setTextInfo(PiecePuzzle piece) {
            this.forInfo = piece;
            this.isFilled = true;
            this.info.add(this.forInfo.infoChunk).align(1);
            this.forInfo.pieceSelected();
        }

        public void unSetTextInfo() {
            this.isFilled = false;
            this.info.removeActor(this.forInfo.infoChunk);
            this.forInfo.pieceUnselected();
            this.forInfo = null;
        }
    }

    class PiecePuzzle {
        private Image frame;
        public Table infoChunk;
        private Label[] infos;
        private boolean isChoosen;
        private InfoPiece itInfo;
        public String pairName;
        public Stack piece;
        public String[] pieceInfo;
        public String pieceName;
        private Label text;

        public PiecePuzzle() {
            this.pairName = "";
            this.pieceName = "";
            createPiece();
        }

        public PiecePuzzle(String pieceName, String pairName, String[] pieceInfo) {
            this.pairName = pairName;
            this.pieceName = pieceName;
            this.pieceInfo = pieceInfo;
            createPiece();
            fillInInfo();
        }

        private void fillInInfo() {
            this.infos = new Label[(this.pieceInfo.length + 1)];
            this.infoChunk = new Table();
            LabelStyle style = new LabelStyle(Assets.instance.fonts.defaultNormal, Color.BLACK);
            this.infoChunk.add(new Label(this.pieceName, style)).padBottom(10.0f);
            this.infoChunk.row();
            for (int i = 1; i < this.infos.length; i++) {
                this.infos[i] = new Label(this.pieceInfo[i - 1], style);
                this.infos[i].scaleBy(1.5f);
                this.infoChunk.add(this.infos[i]);
                this.infoChunk.row();
            }
        }

        public void createPiece() {
            this.isChoosen = false;
            LabelStyle style = new LabelStyle(Assets.instance.fonts.defaultMedium, Color.BLACK);
            this.frame = new Image(Assets.instance.assetFormulaContent.pieceFrame);
            this.text = new Label(this.pieceName, style);
            this.text.setAlignment(1);
            this.text.setWrap(true);
            this.piece = new Stack();
            this.piece.setSize(Level2Screen.this.puzzleBoardWidth, Level2Screen.this.puzzleBoardHeight);
            this.piece.add(this.frame);
            this.piece.add(this.text);
            this.piece.addListener(new InputListener() {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    if (!(Level2Screen.this.firstInfo.isFilled && Level2Screen.this.secondInfo.isFilled && !PiecePuzzle.this.isChoosen)) {
                        PiecePuzzle.this.isChoosen = !PiecePuzzle.this.isChoosen;
                        if (PiecePuzzle.this.isChoosen) {
                            if (!Level2Screen.this.firstInfo.isFilled) {
                                PiecePuzzle.this.itInfo = Level2Screen.this.firstInfo;
                            } else if (!Level2Screen.this.secondInfo.isFilled) {
                                PiecePuzzle.this.itInfo = Level2Screen.this.secondInfo;
                            }
                            PiecePuzzle.this.itInfo.setTextInfo(PiecePuzzle.this.getSelf());
                        } else {
                            PiecePuzzle.this.itInfo.unSetTextInfo();
                        }
                    }
                    return true;
                }
            });
        }

        public PiecePuzzle getSelf() {
            return this;
        }

        public void pieceSelected() {
            this.frame.setDrawable(new Image(Assets.instance.assetFormulaContent.pieceFrameChoosen).getDrawable());
        }

        public void pieceUnselected() {
            this.frame.setDrawable(new Image(Assets.instance.assetFormulaContent.pieceFrame).getDrawable());
            this.isChoosen = false;
        }
    }

    public Level2Screen(DirectedGame game) {
        super(game);
        this.conver.init();
    }

    private void rebuildStage() {
        this.stackMain = new Stack();
        this.firstInfo = new InfoPiece();
        this.secondInfo = new InfoPiece();
        this.puzzleBoard = buildPuzzleBoard();
        this.pieceInformation = buildPieceInformation();
        this.theDecision = buildFinalDecision();
        this.puzzleGame = new Table();
        this.puzzleGame.background(new Image(Assets.instance.assetBook.background).getDrawable());
        this.stackMain.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.stage.clear();
        this.puzzleGame.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.puzzleGame.add(this.puzzleBoard).size(this.puzzleBoardWidth, this.puzzleBoardHeight).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), 0.0f);
        this.puzzleGame.add(this.pieceInformation).size(this.pieceInformationWidth, this.pieceInformationHeight).pad(Constants.getRealHeight(50.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), Constants.getRealWidth(10.0f));
        this.stackMain.add(this.puzzleGame);
        this.stackMain.add(this.theDecision);
        this.stage.addActor(this.stackMain);
        uiGame.init(2);
    }

    private Table buildPieceInformation() {
        Table table = new Table();
        TextButton btnPilih = new TextButton("PILIH", Assets.instance.skinLibGdx);
        btnPilih.getLabel().setFontScale(2.0f);
        btnPilih.getLabel().setColor(Color.BLACK);
        btnPilih.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                Level2Screen.this.checkAnswer();
            }
        });
        table.setSize(this.pieceInformationWidth, this.pieceInformationHeight);
        table.add(this.firstInfo.info).size(Constants.getRealWidth(350.0f), Constants.getRealHeight(200.0f)).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), Constants.getRealWidth(10.0f));
        table.row();
        table.add(this.secondInfo.info).size(Constants.getRealWidth(350.0f), Constants.getRealHeight(200.0f)).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), Constants.getRealWidth(10.0f));
        table.row();
        table.add(btnPilih).size(Constants.getRealWidth(350.0f), Constants.getRealHeight(100.0f)).pad(Constants.getRealHeight(10.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), Constants.getRealWidth(10.0f));
        return table;
    }

    private void checkAnswer() {
        if (this.firstInfo.isFilled && this.secondInfo.isFilled) {
            if (this.firstInfo.forInfo.pieceName.equalsIgnoreCase(this.secondInfo.forInfo.pairName)) {
                this.firstInfo.forInfo.piece.setVisible(false);
                this.secondInfo.forInfo.piece.setVisible(false);
                this.counterAnswerTrue++;
            } else {
                this.penaltyScore++;
                this.timeCountDown -= 5;
                if (this.timeCountDown < 0) {
                    this.timeCountDown = 0;
                }
            }
            this.firstInfo.unSetTextInfo();
            this.secondInfo.unSetTextInfo();
        }
    }

    private Table buildPuzzleBoard() {
        Table table = new Table();
        table.setSize(this.puzzleBoardWidth, this.puzzleBoardHeight);
        Label levelTitle = new Label((CharSequence) "LEVEL 2 PAIRING", new LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK));
        levelTitle.setAlignment(1);
        table.add(levelTitle).size(Constants.getRealWidth(300.0f), Constants.getRealHeight(70.0f)).colspan(3).pad(0.0f, Constants.getRealWidth(5.0f), 0.0f, Constants.getRealWidth(5.0f)).align(1);
        table.add(buildTimer()).size(Constants.getRealWidth(70.0f), Constants.getRealHeight(70.0f)).align(16);
        table.row();
        int huruf = 1;
        int angka = 0;
        int counter = 0;
        if (Gdx.app.getType() == ApplicationType.Android) {
            angka = 1;
            huruf = 2;
        }
        for (int y = 0; y < 4; y++) {
            for (int x = 0; x < 4; x++) {
                String piece;
                String pair;
                String[] infos;
                String[] tempS = this.puzzle[counter].split("");
                if (tempS[huruf].equalsIgnoreCase("a")) {
                    piece = ((PairPiece) this.penyanggaList.getPasanganPenyangga().get(Integer.valueOf(tempS[angka]).intValue() - 1)).getAsamKonjugasi();
                    pair = ((PairPiece) this.penyanggaList.getPasanganPenyangga().get(Integer.valueOf(tempS[angka]).intValue() - 1)).getBasaKonjugasi();
                    infos = ((PairPiece) this.penyanggaList.getPasanganPenyangga().get(Integer.valueOf(tempS[angka]).intValue() - 1)).getInfoAsam();
                } else {
                    piece = ((PairPiece) this.penyanggaList.getPasanganPenyangga().get(Integer.valueOf(tempS[angka]).intValue() - 1)).getBasaKonjugasi();
                    pair = ((PairPiece) this.penyanggaList.getPasanganPenyangga().get(Integer.valueOf(tempS[angka]).intValue() - 1)).getAsamKonjugasi();
                    infos = ((PairPiece) this.penyanggaList.getPasanganPenyangga().get(Integer.valueOf(tempS[angka]).intValue() - 1)).getInfoBasa();
                }
                table.add(new PiecePuzzle(piece, pair, infos).piece).size(Constants.getRealWidth(210.0f), Constants.getRealHeight(130.0f)).pad(Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f));
                counter++;
            }
            table.row();
        }
        return table;
    }

    private Table buildTimer() {
        Table table = new Table();
        this.timer = new Label("" + this.timeCountDown + "s", new LabelStyle(Assets.instance.fonts.defaultBig, Color.BLACK));
        this.timer.setAlignment(8);
        this.timerTask = new Task() {
            public void run() {
                Level2Screen.this.timeCountDown = Level2Screen.this.timeCountDown - 1;
            }
        };
        table.add(this.timer).align(8).size(Constants.getRealWidth(70.0f), Constants.getRealHeight(70.0f)).pad(0.0f, Constants.getRealWidth(5.0f), Constants.getRealHeight(5.0f), Constants.getRealWidth(5.0f));
        table.add();
        return table;
    }

    private Table buildFinalDecision() {
        Table table = new Table();
        table.setSize(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.decision = new Label("", new LabelStyle(Assets.instance.fonts.defaultBig, Color.RED));
        this.finalScore = new Label("", new LabelStyle(Assets.instance.fonts.defaultBig, Color.RED));
        this.decision.setFontScale(2.0f);
        table.center().add(this.decision);
        table.row();
        table.add(this.finalScore);
        return table;
    }

    public void update(float deltaTime) {
        this.timer.setText("" + this.timeCountDown + "s");
        if (this.timeCountDown <= 0) {
            this.finalDecision = Decision.LOSE;
            this.timerTask.cancel();
        } else if (this.counterAnswerTrue == this.penyanggaList.getJumlahPasangan()) {
            this.finalDecision = Decision.WIN;
            this.timerTask.cancel();
        } else {
            this.theDecision.setVisible(false);
        }
        if (this.finalDecision != Decision.PLAY) {
            Gdx.input.setInputProcessor(null);
            this.theDecision.setVisible(true);
            if (this.finalDecision == Decision.WIN) {
                this.decision.setText("WIN!!");
                this.decision.getStyle().fontColor = Color.GREEN;
                this.finalScore.getStyle().fontColor = Color.GREEN;
            } else {
                this.decision.setText("LOSE!!");
                this.decision.getStyle().fontColor = Color.RED;
                this.finalScore.getStyle().fontColor = Color.RED;
            }
            Constants.finalScoreLevel2 = 100.0f - ((((float) this.penaltyScore) / ((float) this.penyanggaList.getJumlahPasangan())) * 100.0f);
            this.finalScore.setText(new DecimalFormat("##.##").format((double) Constants.finalScoreLevel2) + "%");
            this.timeleftForNextScreen -= deltaTime;
            if (this.timeleftForNextScreen < 0.0f) {
                if (this.finalDecision == Decision.WIN) {
                    openSessionBook();
                }
                if (this.finalDecision == Decision.LOSE) {
                    restart();
                }
                this.timeleftForNextScreen = 3.0f;
            }
        }
    }

    private void restart() {
        this.game.setScreen(new Level2Screen(this.game), Constants.screenSlide);
    }

    private void openSessionBook() {
        this.game.setScreen(new SesiBukuScreen(this.game, 2, new Level3Screen(this.game)), Constants.screenSlide);
    }

    public void render(float deltaTime) {
        if (this.conver.isConversationDone) {
            if (this.startTimer) {
                this.startTimer = false;
                Timer.schedule(this.timerTask, 1.0f, 1.0f);
//                Gdx.input.setInputProcessor(this.stage);
            }
            if (!uiGame.onPause) {
                update(deltaTime);
                Timer.instance().start();
            }else{
                Timer.instance().stop();
            }
            if (uiGame.toMenu)
                backToMenu();
        }
        Gdx.gl.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (this.debugEnabled) {
            this.debugRebuildStage -= deltaTime;
            if (this.debugRebuildStage <= 0.0f) {
                this.debugRebuildStage = 5.0f;
                rebuildStage();
            }
        }
        this.stage.act(deltaTime);
        this.stage.draw();
        uiGame.draw();
        this.stage.setDebugAll(this.debugEnabled);
        if (this.conver.isConversationDone) {
            this.paused = false;
            return;
        }
        this.conver.render();
        if (this.paused) {
            this.conver.beginConversation(new Image(Assets.instance.assetBook.background));
        }
    }

    public void resize(int width, int height) {
        this.stage.getViewport().update(width, height);
        this.conver.resize((float) width, (float) height);
        this.cameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
        this.cameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT / ((float) height)) * ((float) width);
        this.cameraGUI.position.set(this.cameraGUI.viewportWidth / 2.0f, this.cameraGUI.viewportHeight / 2.0f, 0.0f);
        this.cameraGUI.update();
        uiGame.resize(width, height);
    }

    public void show() {
        this.paused = true;
        this.conver.setPanels("level2");
        this.stage = new Stage(new StretchViewport(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT));
        this.timeCountDown = 60;
        this.timeleftForNextScreen = 3.0f;
        batch = new SpriteBatch();
        this.cameraGUI = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.cameraGUI.position.set(0.0f, 0.0f, 0.0f);
        this.cameraGUI.setToOrtho(true);
        this.cameraGUI.update();
        uiGame = new UIGame(cameraGUI, batch);
        rebuildStage();
    }

    public void hide() {
        this.stage.dispose();
        this.conver.dispose();
        uiGame.dispose();
        batch.dispose();
    }

    public void pause() {
        this.paused = true;
        this.timerTask.cancel();
    }

    public void resume() {
        super.resume();
        this.paused = false;
    }

    public InputProcessor getInputProcessor() {
        if (this.conver.isConversationDone) {
            InputMultiplexer multiplexer = new InputMultiplexer();
            if (!uiGame.onPause)
                multiplexer.addProcessor(stage);
            multiplexer.addProcessor(uiGame.getInputProcessor());
            return multiplexer;
        }
        return this.conver.getInputProcessor();
    }

    public void backToMenu() {
        this.game.setScreen(new MenuScreen(this.game));
    }
}
