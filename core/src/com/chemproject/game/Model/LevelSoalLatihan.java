package com.chemproject.game.Model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Json;
import java.util.Iterator;

public class LevelSoalLatihan {
    private int jumlahLevel;
    private Array<String> pathSoalLevel;
    private ArrayMap<Integer, PerLevelSoal> soals;

    public static class PerLevelSoal {
        int levelSoal;
        Array<SoalLatihanData> soalsoal;

        public PerLevelSoal() {
        }

        public PerLevelSoal(int levelSoal, Array<SoalLatihanData> soalsoal) {
            this.levelSoal = levelSoal;
            this.soalsoal = soalsoal;
        }

        public int getLevelSoal() {
            return this.levelSoal;
        }

        public void setLevelSoal(int levelSoal) {
            this.levelSoal = levelSoal;
        }

        public Array<SoalLatihanData> getSoalsoal() {
            return this.soalsoal;
        }

        public void setSoalsoal(Array<SoalLatihanData> soalsoal) {
            this.soalsoal = soalsoal;
        }
    }

    public LevelSoalLatihan() {
    }

    public LevelSoalLatihan(int jumlahLevel, Array<String> pathSoalLevel) {
        this.jumlahLevel = jumlahLevel;
        this.pathSoalLevel = pathSoalLevel;
    }

    public void init() {
        Json json = new Json();
        this.soals = new ArrayMap();
        Iterator it = this.pathSoalLevel.iterator();
        while (it.hasNext()) {
            PerLevelSoal temp = (PerLevelSoal) json.fromJson(PerLevelSoal.class, Gdx.files.internal((String) it.next()));
            this.soals.put(Integer.valueOf(temp.getLevelSoal()), temp);
        }
    }

    public int getJumlahLevel() {
        return this.jumlahLevel;
    }

    public void setJumlahLevel(int jumlahLevel) {
        this.jumlahLevel = jumlahLevel;
    }

    public Array<String> getPathSoalLevel() {
        return this.pathSoalLevel;
    }

    public void setPathSoalLevel(Array<String> pathSoalLevel) {
        this.pathSoalLevel = pathSoalLevel;
    }

    public ArrayMap<Integer, PerLevelSoal> getSoals() {
        return this.soals;
    }

    public void setSoals(ArrayMap<Integer, PerLevelSoal> soals) {
        this.soals = soals;
    }
}
