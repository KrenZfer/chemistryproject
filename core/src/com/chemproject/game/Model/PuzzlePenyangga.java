package com.chemproject.game.Model;

import com.badlogic.gdx.utils.Array;

public class PuzzlePenyangga {
    int jumlahPasangan;
    Array<PairPiece> pasanganPenyangga;

    public PuzzlePenyangga() {
    }

    public PuzzlePenyangga(Array<PairPiece> pasanganPenyangga, int jumlahPasangan) {
        this.pasanganPenyangga = pasanganPenyangga;
        this.jumlahPasangan = jumlahPasangan;
    }

    public Array<PairPiece> getPasanganPenyangga() {
        return this.pasanganPenyangga;
    }

    public void setPasanganPenyangga(Array<PairPiece> pasanganPenyangga) {
        this.pasanganPenyangga = pasanganPenyangga;
    }

    public int getJumlahPasangan() {
        return this.jumlahPasangan;
    }

    public void setJumlahPasangan(int jumlahPasangan) {
        this.jumlahPasangan = jumlahPasangan;
    }
}
