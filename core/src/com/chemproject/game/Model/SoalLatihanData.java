package com.chemproject.game.Model;

import com.badlogic.gdx.utils.Array;

public class SoalLatihanData {
    int indeksJawabanBenar;
    Array<String> jawabanSoal;
    int noSoal;
    String textSoal;

    public SoalLatihanData() {
    }

    public SoalLatihanData(int noSoal, String textSoal, Array<String> jawabanSoal, int indeksJawabanBenar) {
        this.noSoal = noSoal;
        this.textSoal = textSoal;
        this.jawabanSoal = jawabanSoal;
        this.indeksJawabanBenar = indeksJawabanBenar;
    }

    public int getIndeksJawabanBenar() {
        return this.indeksJawabanBenar;
    }

    public void setIndeksJawabanBenar(int indeksJawabanBenar) {
        this.indeksJawabanBenar = indeksJawabanBenar;
    }

    public int getNoSoal() {
        return this.noSoal;
    }

    public void setNoSoal(int noSoal) {
        this.noSoal = noSoal;
    }

    public String getTextSoal() {
        return this.textSoal;
    }

    public void setTextSoal(String textSoal) {
        this.textSoal = textSoal;
    }

    public Array<String> getJawabanSoal() {
        return this.jawabanSoal;
    }

    public void setJawabanSoal(Array<String> jawabanSoal) {
        this.jawabanSoal = jawabanSoal;
    }
}
