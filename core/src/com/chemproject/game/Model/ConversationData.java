package com.chemproject.game.Model;

import com.badlogic.gdx.utils.Array;

public class ConversationData {
    private Array<Data> dataJsonConversation;
    private int totalScene;

    public static class Data {
        private int id;
        private String jsonConversation;

        public Data() {
        }

        public Data(String jsonConversation, int id) {
            this.jsonConversation = jsonConversation;
            this.id = id;
        }

        public String getJsonConversation() {
            return this.jsonConversation;
        }

        public void setJsonConversation(String jsonConversation) {
            this.jsonConversation = jsonConversation;
        }

        public int getId() {
            return this.id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public ConversationData() {
    }

    public ConversationData(Array<Data> dataJsonConversation, int totalScene) {
        this.dataJsonConversation = dataJsonConversation;
        this.totalScene = totalScene;
    }

    public Array<Data> getDataJsonConversation() {
        return this.dataJsonConversation;
    }

    public void setDataJsonConversation(Array<Data> dataJsonConversation) {
        this.dataJsonConversation = dataJsonConversation;
    }

    public int getTotalScene() {
        return this.totalScene;
    }

    public void setTotalScene(int totalScene) {
        this.totalScene = totalScene;
    }
}
