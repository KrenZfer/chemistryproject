package com.chemproject.game.Model;

import com.badlogic.gdx.utils.Array;

public class TeksPanduan {

    private Array<TeksRinci> teksArray;

    public TeksPanduan() {
    }

    public TeksPanduan(Array<TeksRinci> teksArray) {
        this.teksArray = teksArray;
    }

    public Array<TeksRinci> getTeksArray() {
        return teksArray;
    }

    public void setTeksArray(Array<TeksRinci> teksArray) {
        this.teksArray = teksArray;
    }

    public static class TeksRinci {
        private int level;
        private Array<String> teks;

        public TeksRinci() {
        }

        public TeksRinci(int level, Array<String> teks) {
            this.level = level;
            this.teks = teks;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public Array<String> getTeks() {
            return teks;
        }

        public void setTeks(Array<String> teks) {
            this.teks = teks;
        }
    }
}
