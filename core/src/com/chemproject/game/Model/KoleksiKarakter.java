package com.chemproject.game.Model;

import com.badlogic.gdx.utils.Array;

public class KoleksiKarakter {
    private Array<Karakter> koleksi;
    private int totalKarakter;

    public static class Karakter {
        private int id;
        private String nama;
        private String path;

        public Karakter() {
        }

        public Karakter(int id, String nama, String path) {
            this.id = id;
            this.nama = nama;
            this.path = path;
        }

        public int getId() {
            return this.id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getNama() {
            return this.nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getPath() {
            return this.path;
        }

        public void setPath(String path) {
            this.path = path;
        }
    }

    public KoleksiKarakter() {
    }

    public KoleksiKarakter(Array<Karakter> koleksi) {
        this.koleksi = koleksi;
        this.totalKarakter = koleksi.size;
    }

    public int getTotalKarakter() {
        return this.totalKarakter;
    }

    public void setTotalKarakter(int totalKarakter) {
        this.totalKarakter = totalKarakter;
    }

    public Array<Karakter> getKoleksi() {
        return this.koleksi;
    }

    public void setKoleksi(Array<Karakter> koleksi) {
        this.koleksi = koleksi;
    }
}
