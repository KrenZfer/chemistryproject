package com.chemproject.game.Model;

public class Conversation {
    private int idConversation;
    private String imagePerson;
    private String namePerson;
    private String objectConversation;
    private String panelAlign;
    private String textConversation;

    public Conversation(String textConversation, String namePerson, String objectConversation, String panelAlign, String imagePerson, int idConversation) {
        this.textConversation = textConversation;
        this.namePerson = namePerson;
        this.objectConversation = objectConversation;
        this.panelAlign = panelAlign;
        this.imagePerson = imagePerson;
        this.idConversation = idConversation;
    }

    public Conversation() {
    }

    public String getTextConversation() {
        return this.textConversation;
    }

    public void setTextConversation(String textConversation) {
        this.textConversation = textConversation;
    }

    public String getNamePerson() {
        return this.namePerson;
    }

    public void setNamePerson(String namePerson) {
        this.namePerson = namePerson;
    }

    public int getIdConversation() {
        return this.idConversation;
    }

    public void setIdConversation(int idConversation) {
        this.idConversation = idConversation;
    }

    public String getObjectConversation() {
        return this.objectConversation;
    }

    public void setObjectConversation(String objectConversation) {
        this.objectConversation = objectConversation;
    }

    public String getPanelAlign() {
        return this.panelAlign;
    }

    public void setPanelAlign(String panelAlign) {
        this.panelAlign = panelAlign;
    }

    public String getImagePerson() {
        return this.imagePerson;
    }

    public void setImagePerson(String imagePerson) {
        this.imagePerson = imagePerson;
    }
}
