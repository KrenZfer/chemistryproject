package com.chemproject.game.Model;

public class PairPiece {
    private String asamKonjugasi;
    private String basaKonjugasi;
    private String[] infoAsam;
    private String[] infoBasa;

    public PairPiece() {
    }

    public PairPiece(String asamKonjugasi, String basaKonjugasi, String[] infoAsam, String[] infoBasa) {
        this.asamKonjugasi = asamKonjugasi;
        this.basaKonjugasi = basaKonjugasi;
        this.infoAsam = infoAsam;
        this.infoBasa = infoBasa;
    }

    public String getAsamKonjugasi() {
        return this.asamKonjugasi;
    }

    public void setAsamKonjugasi(String asamKonjugasi) {
        this.asamKonjugasi = asamKonjugasi;
    }

    public String getBasaKonjugasi() {
        return this.basaKonjugasi;
    }

    public void setBasaKonjugasi(String basaKonjugasi) {
        this.basaKonjugasi = basaKonjugasi;
    }

    public String[] getInfoAsam() {
        return this.infoAsam;
    }

    public void setInfoAsam(String[] infoAsam) {
        this.infoAsam = infoAsam;
    }

    public String[] getInfoBasa() {
        return this.infoBasa;
    }

    public void setInfoBasa(String[] infoBasa) {
        this.infoBasa = infoBasa;
    }
}
