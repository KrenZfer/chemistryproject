package com.chemproject.game.Model;

import com.badlogic.gdx.utils.Array;

public class SceneConversation {
    private Array<Conversation> conversations;
    private String sceneName;

    public SceneConversation() {
    }

    public SceneConversation(String sceneName, Array<Conversation> conversations) {
        this.sceneName = sceneName;
        this.conversations = conversations;
    }

    public String getSceneName() {
        return this.sceneName;
    }

    public void setSceneName(String sceneName) {
        this.sceneName = sceneName;
    }

    public Array<Conversation> getConversations() {
        return this.conversations;
    }

    public void setConversations(Array<Conversation> conversations) {
        this.conversations = conversations;
    }
}
