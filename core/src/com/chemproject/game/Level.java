package com.chemproject.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.chemproject.game.ObjectGame.AbstractGameObject;
import com.chemproject.game.ObjectGame.BunnyHead;
import com.chemproject.game.ObjectGame.ObjectClouds;
import com.chemproject.game.ObjectGame.ObjectLarutan;
import com.chemproject.game.ObjectGame.ObjectLarutan.Type;
import com.chemproject.game.ObjectGame.ObjectMountain;
import com.chemproject.game.ObjectGame.ObjectPipa;
import com.chemproject.game.ObjectGame.ObjectRock;
import com.chemproject.game.ObjectGame.ObjectSemak;
import com.chemproject.game.ObjectGame.ObjectWaterOverlay;
import com.chemproject.game.ObjectGame.PortalPoint;
import com.chemproject.game.ObjectGame.PortalPoint.Portal;
import com.chemproject.game.Utils.Assets;
import java.util.Iterator;

public class Level {
    public static final String TAG = Level.class.getName();
    public BunnyHead bunnyHead;
    public ObjectClouds clouds;
    public Vector2 end;
    public PortalPoint endPoint;
    public Array<ObjectLarutan> larutan;
    private final float mapScale = 1.5f;
    public ObjectMountain mountains;
    public Array<String> namaLarutan;
    public Array<ObjectPipa> pipas;
    public Array<ObjectRock> rocks;
    public Array<ObjectSemak> semaks;
    public PortalPoint startPoint;
    public ObjectWaterOverlay waterOverlay;

    public enum BLOCK_TYPE {
        EMPTY(0, 0, 0),
        ROCK(0, 255, 0),
        PLAYER_SPAWNPOINT(255, 255, 255),
        ITEM_LARUTAN(255, 255, 0),
        PLAYER_ENDPOINT(255, 0, 0),
        SEMAK(0, 0, 255),
        PIPA(255, 0, 255);
        
        private int color;

        private BLOCK_TYPE(int r, int g, int b) {
            this.color = (((r << 24) | (g << 16)) | (b << 8)) | 255;
        }

        public boolean sameColor(int color) {
            return this.color == color;
        }

        public int getColor() {
            return this.color;
        }
    }

    public Level(String fileName) {
        init(fileName);
    }

    private void init(String fileName) {
        this.bunnyHead = null;
        this.rocks = new Array();
        this.larutan = new Array();
        this.namaLarutan = new Array();
        this.pipas = new Array();
        this.semaks = new Array();
        Pixmap pixmap = new Pixmap(Gdx.files.internal(fileName));
        int lastPixel = -1;
        for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {
            for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {
                float baseHeight = (float) (pixmap.getHeight() - pixelY);
                int currentPixel = pixmap.getPixel(pixelX, pixelY);
                if (!BLOCK_TYPE.EMPTY.sameColor(currentPixel)) {
                    AbstractGameObject obj;
                    if (BLOCK_TYPE.ROCK.sameColor(currentPixel)) {
                        if (lastPixel != currentPixel) {
                            obj = new ObjectRock();
                            obj.position.set((float) pixelX, ((1.5f * baseHeight) * 1.0f) - 1.5f);
                            this.rocks.add((ObjectRock) obj);
                        } else {
                            ((ObjectRock) this.rocks.get(this.rocks.size - 1)).increaseLength(1);
                        }
                    } else if (BLOCK_TYPE.PLAYER_SPAWNPOINT.sameColor(currentPixel)) {
                        obj = new BunnyHead();
                        obj.position.set((float) pixelX, (1.5f * baseHeight) + 0.0f);
                        this.bunnyHead = (BunnyHead) obj;
                        obj = new PortalPoint(Portal.IN);
                        obj.position.set((float) pixelX, (1.5f * baseHeight) - 1.5f);
                        this.startPoint = (PortalPoint) obj;
                    } else if (BLOCK_TYPE.ITEM_LARUTAN.sameColor(currentPixel)) {
                        AtlasRegion reg;
                        Type type;
                        do {
                            if (MathUtils.randomBoolean()) {
                                reg = (AtlasRegion) Assets.instance.assetFormulaContent.larutanPenyangga.random();
                                type = Type.PENYANGGA;
                            } else {
                                reg = (AtlasRegion) Assets.instance.assetFormulaContent.larutanBukanPenyangga.random();
                                type = Type.BUKANPENYANGGA;
                            }
                            if (reg != null && this.namaLarutan.contains(reg.name, false)) {
                                reg = null;
                                continue;
                            }
                        } while (reg == null);
                        this.namaLarutan.add(reg.name);
                        obj = new ObjectLarutan(type, reg);
                        obj.position.set((float) pixelX, (1.5f * baseHeight) - 1.5f);
                        this.larutan.add((ObjectLarutan) obj);
                    } else if (BLOCK_TYPE.PLAYER_ENDPOINT.sameColor(currentPixel)) {
                        this.end = new Vector2();
                        this.end.set((float) pixelX, (1.5f * baseHeight) - 1.5f);
                        obj = new PortalPoint(Portal.IN);
                        obj.position.set(this.end);
                        this.endPoint = (PortalPoint) obj;
                    } else if (BLOCK_TYPE.PIPA.sameColor(currentPixel)) {
                        obj = new ObjectPipa();
                        obj.position.set((float) pixelX, (1.5f * baseHeight) - 1.5f);
                        this.pipas.add((ObjectPipa) obj);
                    } else if (BLOCK_TYPE.SEMAK.sameColor(currentPixel)) {
                        obj = new ObjectSemak();
                        obj.position.set((float) pixelX, (1.5f * baseHeight) - 1.5f);
                        this.semaks.add((ObjectSemak) obj);
                    } else {
                        int g = (currentPixel >>> 16) & 255;
                        int b = (currentPixel >>> 8) & 255;
                        int a = currentPixel & 255;
                        Gdx.app.error(TAG, "Uknown object at x<" + pixelX + "> y<" + pixelY + ">: r<" + ((currentPixel >>> 24) & 255) + "> g<" + g + "> b<" + b + "> a<" + a + ">");
                    }
                }
                lastPixel = currentPixel;
            }
        }
        this.clouds = new ObjectClouds((float) pixmap.getWidth(), 7.0f);
        this.mountains = new ObjectMountain(pixmap.getWidth());
        this.mountains.position.set(0.0f, -1.5f);
        this.waterOverlay = new ObjectWaterOverlay((float) pixmap.getWidth());
        this.waterOverlay.position.set(0.0f, -4.75f);
        pixmap.dispose();
        Gdx.app.debug(TAG, "Level '" + fileName + "' loaded");
    }

    public void render(SpriteBatch batch) {
        this.mountains.render(batch);
        Iterator it = this.rocks.iterator();
        while (it.hasNext()) {
            ((ObjectRock) it.next()).render(batch);
        }
        it = this.larutan.iterator();
        while (it.hasNext()) {
            ((ObjectLarutan) it.next()).render(batch);
        }
        it = this.semaks.iterator();
        while (it.hasNext()) {
            ((ObjectSemak) it.next()).render(batch);
        }
        it = this.pipas.iterator();
        while (it.hasNext()) {
            ((ObjectPipa) it.next()).render(batch);
        }
        this.startPoint.render(batch);
        this.endPoint.render(batch);
        this.bunnyHead.render(batch);
        this.waterOverlay.render(batch);
        this.clouds.render(batch);
    }

    public void update(float deltaTime) {
        this.bunnyHead.update(deltaTime);
        Iterator it = this.rocks.iterator();
        while (it.hasNext()) {
            ((ObjectRock) it.next()).update(deltaTime);
        }
        this.clouds.update(deltaTime);
    }
}
