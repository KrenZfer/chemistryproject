package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public abstract class AbstractGameObject {
    public Vector2 acceleration = new Vector2();
    public Rectangle bounds = new Rectangle();
    public Vector2 dimension = new Vector2(1.0f, 1.0f);
    public Vector2 friction = new Vector2();
    public Vector2 origin = new Vector2();
    public Vector2 position = new Vector2();
    public float rotation = 0.0f;
    public Vector2 scale = new Vector2(1.0f, 1.0f);
    public Vector2 terminalVelocity = new Vector2(1.0f, 1.0f);
    public Vector2 velocity = new Vector2();

    public abstract void render(SpriteBatch spriteBatch);

    public void update(float deltaTime) {
        updateMotionX(deltaTime);
        updateMotionY(deltaTime);
        Vector2 vector2 = this.position;
        vector2.x += this.velocity.x * deltaTime;
        vector2 = this.position;
        vector2.y += this.velocity.y * deltaTime;
    }

    protected void updateMotionX(float deltaTime) {
        if (this.velocity.x != 0.0f) {
            if (this.velocity.x > 0.0f) {
                this.velocity.x = Math.max(this.velocity.x - (this.friction.x * deltaTime), 0.0f);
            } else {
                this.velocity.x = Math.min(this.velocity.x + (this.friction.x * deltaTime), 0.0f);
            }
        }
        Vector2 vector2 = this.velocity;
        vector2.x += this.acceleration.x * deltaTime;
        this.velocity.x = MathUtils.clamp(this.velocity.x, -this.terminalVelocity.x, this.terminalVelocity.x);
    }

    protected void updateMotionY(float deltaTime) {
        if (this.velocity.y != 0.0f) {
            if (this.velocity.y > 0.0f) {
                this.velocity.y = Math.max(this.velocity.y - (this.friction.y * deltaTime), 0.0f);
            } else {
                this.velocity.y = Math.min(this.velocity.y + (this.friction.y * deltaTime), 0.0f);
            }
        }
        Vector2 vector2 = this.velocity;
        vector2.y += this.acceleration.y * deltaTime;
        this.velocity.y = MathUtils.clamp(this.velocity.y, -this.terminalVelocity.y, this.terminalVelocity.y);
    }
}
