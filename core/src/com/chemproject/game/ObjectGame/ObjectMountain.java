package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.chemproject.game.Utils.Assets;

public class ObjectMountain extends AbstractGameObject {
    private int length;
    private TextureRegion regMountain1;
    private TextureRegion regMountain2;
    private TextureRegion regMountain3;

    public ObjectMountain(int length) {
        this.length = length;
        init();
    }

    public void init() {
        this.dimension.set(18.5f, 5.0f);
        this.regMountain1 = Assets.instance.levelDecoration.mountain1;
        this.regMountain2 = Assets.instance.levelDecoration.mountain2;
        this.regMountain3 = Assets.instance.levelDecoration.mountain3;
        this.origin.x = (-this.dimension.x) * 2.0f;
        this.length = (int) (((float) this.length) + (this.dimension.x * 2.0f));
    }

    private TextureRegion getMountain(int indeks) {
        if (indeks == 1) {
            return this.regMountain1;
        }
        if (indeks == 2) {
            return this.regMountain2;
        }
        if (indeks == 3) {
            return this.regMountain3;
        }
        return null;
    }

    public void drawMountain(SpriteBatch batch, int indeks, float offsetX, float offsetY, float tintColor, float parallaxSpeedX) {
        batch.setColor(tintColor, tintColor, tintColor, 1.0f);
        float relX = this.dimension.x * offsetX;
        float relY = this.dimension.y * offsetY;
        int mountainLength = (0 + MathUtils.ceil((((float) this.length) / (2.0f * this.dimension.x)) * (1.0f - parallaxSpeedX))) + MathUtils.ceil(0.5f + offsetX);
        for (int i = 0; i < mountainLength; i++) {
            TextureRegion reg = getMountain(indeks);
            SpriteBatch spriteBatch = batch;
            spriteBatch.draw(reg.getTexture(), (this.position.x * parallaxSpeedX) + (this.origin.x + relX), (this.position.y + this.origin.y) + relY, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
            relX += this.dimension.x;
            spriteBatch = batch;
            spriteBatch.draw(reg.getTexture(), (this.position.x * parallaxSpeedX) + (this.origin.x + relX), (this.position.y + this.origin.y) + relY, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), true, false);
            relX += this.dimension.x;
        }
        batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    public void updateScrollPosition(Vector2 camPosition) {
        this.position.set(camPosition.x, this.position.y);
    }

    public void render(SpriteBatch batch) {
        drawMountain(batch, 2, 0.5f, 0.6f, 1.0f, 0.8f);
        drawMountain(batch, 3, 0.25f, 0.2f, 1.0f, 0.5f);
        drawMountain(batch, 1, 0.0f, 0.0f, 1.0f, 0.3f);
    }
}
