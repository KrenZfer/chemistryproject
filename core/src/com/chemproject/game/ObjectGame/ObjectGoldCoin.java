package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;

public class ObjectGoldCoin extends AbstractGameObject {
    public boolean collected;
    private TextureRegion regGoldCoin;

    public ObjectGoldCoin() {
        init();
    }

    private void init() {
        this.dimension.set(1.0f, 1.0f);
        this.regGoldCoin = Assets.instance.goldCoin.coin;
        this.bounds.set(0.0f, 0.0f, this.dimension.x, this.dimension.y);
        this.collected = false;
    }

    public void render(SpriteBatch batch) {
        if (!this.collected) {
            TextureRegion reg = this.regGoldCoin;
            batch.draw(reg.getTexture(), this.position.x, this.position.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x / 2.0f, this.scale.y / 2.0f, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
        }
    }

    public int getScore() {
        return 100;
    }
}
