package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;

public class ObjectFeather extends AbstractGameObject {
    public boolean collected;
    private TextureRegion regFeather;

    public ObjectFeather() {
        init();
    }

    public void init() {
        this.dimension.set(0.5f, 0.5f);
        this.regFeather = Assets.instance.feather.feather;
        this.bounds.set(0.0f, 0.0f, this.dimension.x, this.dimension.x);
        this.collected = false;
    }

    public void render(SpriteBatch batch) {
        if (!this.collected) {
            TextureRegion reg = this.regFeather;
            batch.draw(reg.getTexture(), this.position.x, this.position.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
        }
    }

    public int getScore() {
        return Keys.F7;
    }
}
