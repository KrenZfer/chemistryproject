package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;

public class ObjectWaterOverlay extends AbstractGameObject {
    private float length;
    private TextureRegion regWaterOverlay;

    public ObjectWaterOverlay(float length) {
        this.length = length;
        init();
    }

    public void init() {
        this.dimension.set(this.length * 10.0f, 5.0f);
        this.regWaterOverlay = Assets.instance.levelDecoration.waterOverlay;
        this.origin.x = (-this.dimension.x) / 2.0f;
    }

    public void render(SpriteBatch batch) {
        TextureRegion reg = this.regWaterOverlay;
        SpriteBatch spriteBatch = batch;
        spriteBatch.draw(reg.getTexture(), this.origin.x + this.position.x, this.origin.y + this.position.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
    }
}
