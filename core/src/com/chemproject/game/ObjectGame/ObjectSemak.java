package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;

public class ObjectSemak extends AbstractGameObject {
    TextureRegion reg = Assets.instance.levelDecoration.semak;

    public ObjectSemak() {
        this.dimension.set(2.0f, 1.0f);
    }

    public void render(SpriteBatch batch) {
        batch.draw(this.reg.getTexture(), this.position.x + this.origin.x, this.position.y + this.origin.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, this.reg.getRegionX(), this.reg.getRegionY(), this.reg.getRegionWidth(), this.reg.getRegionHeight(), false, false);
    }
}
