package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;

public class PortalPoint extends AbstractGameObject {
    public boolean isActive = false;
    Portal portal;
    TextureRegion regPortal = Assets.instance.levelDecoration.portal;

    public enum Portal {
        IN,
        OUT
    }

    public void activate() {
        this.isActive = true;
    }

    public PortalPoint(Portal portal) {
        this.portal = portal;
        this.dimension.set(1.0f, 2.0f);
    }

    public void render(SpriteBatch batch) {
        TextureRegion reg = this.regPortal;
        batch.draw(reg.getTexture(), this.position.x, this.position.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
    }
}
