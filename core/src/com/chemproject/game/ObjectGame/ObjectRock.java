package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.chemproject.game.Utils.Assets;

public class ObjectRock extends AbstractGameObject {
    private final float FLOAT_AMPLITUDE = 0.25f;
    private final float FLOAT_CYCLE_TIME = 2.0f;
    private float floatCycleTimeLeft;
    private Vector2 floatTargetPosition;
    private boolean floatingDownwards;
    private int length;
    private TextureRegion regEdge;
    private TextureRegion regMiddle;

    public ObjectRock() {
        init();
    }

    private void init() {
        this.dimension.set(1.0f, 1.5f);
        this.regEdge = Assets.instance.rock.edge;
        this.regMiddle = Assets.instance.rock.middle;
        setLength(1);
        this.floatingDownwards = false;
        this.floatCycleTimeLeft = MathUtils.random(0.0f, 1.0f);
        this.floatTargetPosition = null;
    }

    public void render(SpriteBatch batch) {
        TextureRegion reg = this.regEdge;
        batch.draw(reg.getTexture(), this.position.x + 0.0f, this.position.y + 0.0f, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
        reg = this.regMiddle;
        float relX = 0.0f + this.dimension.x;
        for (int i = 0; i < this.length - 2; i++) {
            batch.draw(reg.getTexture(), this.position.x + relX, this.position.y + 0.0f, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
            relX += this.dimension.x;
        }
        reg = this.regEdge;
        batch.draw(reg.getTexture(), this.position.x + relX, this.position.y + 0.0f, this.origin.x / 4.0f, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), true, false);
    }

    public void setLength(int length) {
        this.length = length;
        this.bounds.set(0.0f, 0.0f, this.dimension.x * ((float) length), this.dimension.y);
    }

    public void increaseLength(int amount) {
        setLength(this.length + amount);
    }

    public void update(float deltaTime) {
        super.update(deltaTime);
    }
}
