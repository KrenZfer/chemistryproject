package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class ObjectLarutan extends AbstractGameObject {
    public boolean isCollected = false;
    TextureRegion regLarutan;
    public Type type;

    public enum Type {
        PENYANGGA,
        BUKANPENYANGGA
    }

    public ObjectLarutan(Type type, TextureRegion larutan) {
        this.type = type;
        this.regLarutan = larutan;
        this.dimension.set(2.0f, 0.3f);
        this.bounds.set(this.dimension.x / 4.0f, 0.0f, this.dimension.x / 2.0f, this.dimension.y);
        this.origin.set(this.dimension.x / 2.0f, this.dimension.y / 2.0f);
    }

    public void render(SpriteBatch batch) {
        TextureRegion reg = this.regLarutan;
        if (!this.isCollected) {
            batch.draw(reg.getTexture(), this.position.x, this.position.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
        }
    }
}
