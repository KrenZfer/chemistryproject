package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;

public class ObjectChemSymbol extends AbstractGameObject {
    public boolean isCollected;
    private TextureRegion regChemSymbol;

    public ObjectChemSymbol() {
        init();
    }

    private void init() {
        this.dimension.set(0.5f, 0.5f);
        this.regChemSymbol = Assets.instance.levelDecoration.chemSymbol;
        this.bounds.set(0.0f, 0.0f, this.dimension.x, this.dimension.y);
        this.isCollected = false;
    }

    public void render(SpriteBatch batch) {
        if (!this.isCollected) {
            TextureRegion reg = this.regChemSymbol;
            batch.draw(reg.getTexture(), this.position.x, this.position.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), false, false);
        }
    }
}
