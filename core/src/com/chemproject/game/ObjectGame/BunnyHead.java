package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.AudioManager;
import com.chemproject.game.Utils.CharacterSkin;
import com.chemproject.game.Utils.GamePreferences;

public class BunnyHead extends AbstractGameObject {
    public static final String TAG = BunnyHead.class.getName();
    private final float JUMP_TIME_MAX = 0.3f;
    private final float JUMP_TIME_MIN = 0.1f;
    private final float JUMP_TIME_OFFSET_FLYING = 0.282f;
    public ParticleEffect dustParticle = new ParticleEffect();
    public boolean hasFeatherPowerUp;
    public JUMP_STATE jumpState;
    private TextureRegion regHead;
    public float timeJumping;
    public float timeLeftFeatherPowerUp;
    public VIEW_DIRECTION viewDirection;

    public enum JUMP_STATE {
        GROUNDED,
        FALLING,
        JUMP_RISING,
        JUMP_FALLING
    }

    public enum VIEW_DIRECTION {
        LEFT,
        RIGHT
    }

    public BunnyHead() {
        init();
    }

    public void init() {
        this.dimension.set(0.5f, 1.0f);
        this.regHead = (TextureRegion) Assets.instance.karakter.koleksiKarakter.get("cowokbiasa");
        this.origin.set(this.dimension.x / 2.0f, this.dimension.y / 2.0f);
        this.bounds.set(0.0f, 0.0f, this.dimension.x, this.dimension.y);
        this.terminalVelocity.set(4.0f, 8.0f);
        this.friction.set(12.0f, 0.0f);
        this.acceleration.set(0.0f, -25.0f);
        this.viewDirection = VIEW_DIRECTION.RIGHT;
        this.jumpState = JUMP_STATE.FALLING;
        this.timeJumping = 0.0f;
        this.hasFeatherPowerUp = false;
        this.timeLeftFeatherPowerUp = 0.0f;
        this.dustParticle.load(Gdx.files.internal("particles/dust.pfx"), Gdx.files.internal("particles"));
    }

    public void setJumping(boolean jumpKeyPressed) {
        switch (this.jumpState) {
            case GROUNDED:
                if (jumpKeyPressed) {
                    AudioManager.instance.play(Assets.instance.sounds.jump);
                    this.timeJumping = 0.0f;
                    this.jumpState = JUMP_STATE.JUMP_RISING;
                    return;
                }
                return;
            case JUMP_RISING:
                if (!jumpKeyPressed) {
                    this.jumpState = JUMP_STATE.FALLING;
                    return;
                }
                return;
            case FALLING:
            case JUMP_FALLING:
                if (jumpKeyPressed && this.hasFeatherPowerUp) {
                    this.timeJumping = 0.282f;
                    this.jumpState = JUMP_STATE.JUMP_RISING;
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void setFeatherPowerUp(boolean pickedUp) {
        this.hasFeatherPowerUp = pickedUp;
        if (pickedUp) {
            this.timeLeftFeatherPowerUp = 9.0f;
        }
    }

    public boolean hasFeatherPowerUp() {
        return this.hasFeatherPowerUp && this.timeLeftFeatherPowerUp > 0.0f;
    }

    public void render(SpriteBatch batch) {
        boolean z;
        this.dustParticle.draw(batch);
        batch.setColor(CharacterSkin.values()[GamePreferences.instance.charSkin].getColor());
        if (this.hasFeatherPowerUp) {
            batch.setColor(1.0f, 0.8f, 0.0f, 1.0f);
        }
        TextureRegion reg = this.regHead;
        Texture texture = reg.getTexture();
        float f = this.position.x;
        float f2 = this.position.y;
        float f3 = this.origin.x;
        float f4 = this.origin.y;
        float f5 = this.dimension.x;
        float f6 = this.dimension.y;
        float f7 = this.scale.x;
        float f8 = this.scale.y;
        float f9 = this.rotation;
        int regionX = reg.getRegionX();
        int regionY = reg.getRegionY();
        int regionWidth = reg.getRegionWidth();
        int regionHeight = reg.getRegionHeight();
        if (this.viewDirection == VIEW_DIRECTION.LEFT) {
            z = true;
        } else {
            z = false;
        }
        batch.draw(texture, f, f2, f3, f4, f5, f6, f7, f8, f9, regionX, regionY, regionWidth, regionHeight, z, false);
        batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
    }

    public void update(float deltaTime) {
        super.update(deltaTime);
        if (this.velocity.x != 0.0f) {
            this.viewDirection = this.velocity.x < 0.0f ? VIEW_DIRECTION.LEFT : VIEW_DIRECTION.RIGHT;
        }
        if (this.timeLeftFeatherPowerUp > 0.0f) {
            this.timeLeftFeatherPowerUp -= deltaTime;
            if (this.timeLeftFeatherPowerUp < 0.0f) {
                this.timeLeftFeatherPowerUp = 0.0f;
                setFeatherPowerUp(false);
            }
        }
        this.dustParticle.update(deltaTime);
    }

    protected void updateMotionY(float deltaTime) {
        switch (this.jumpState) {
            case GROUNDED:
                this.jumpState = JUMP_STATE.FALLING;
                if (this.velocity.x != 0.0f) {
                    this.dustParticle.setPosition(this.position.x + (this.dimension.x / 2.0f), this.position.y);
                    this.dustParticle.start();
                    break;
                }
                break;
            case JUMP_RISING:
                this.timeJumping += deltaTime;
                if (this.timeJumping <= 0.3f) {
                    this.velocity.y = this.terminalVelocity.y;
                    break;
                }
                break;
            case JUMP_FALLING:
                this.timeJumping += deltaTime;
                if (this.timeJumping > 0.0f && this.timeJumping <= 0.1f) {
                    this.velocity.y = this.terminalVelocity.y;
                    break;
                }
        }
        if (this.jumpState != JUMP_STATE.GROUNDED) {
            this.dustParticle.allowCompletion();
            super.updateMotionY(deltaTime);
        }
    }
}
