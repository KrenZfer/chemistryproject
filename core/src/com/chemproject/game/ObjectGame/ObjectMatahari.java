package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.chemproject.game.Utils.Assets;

public class ObjectMatahari extends AbstractGameObject {
    private TextureRegion reg = Assets.instance.levelDecoration.matahari;

    public void init() {
        this.dimension.set(5.0f, 5.0f);
    }

    public void render(SpriteBatch batch) {
        batch.draw(this.reg.getTexture(), this.origin.x + this.position.x, this.origin.y + this.position.y, this.origin.x, this.origin.x, this.dimension.x, this.dimension.y, this.scale.x, this.scale.y, this.rotation, this.reg.getRegionX(), this.reg.getRegionY(), this.reg.getRegionWidth(), this.reg.getRegionHeight(), false, false);
    }
}
