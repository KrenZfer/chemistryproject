package com.chemproject.game.ObjectGame;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.chemproject.game.Utils.Assets;
import java.util.Iterator;

public class ObjectClouds extends AbstractGameObject {
    private Array<Cloud> clouds;
    private float length;
    private TextureRegion regCloud;

    private class Cloud extends AbstractGameObject {
        boolean flipx;
        private TextureRegion regCloud;
        float scaleCloud;

        public Cloud(boolean flipx, float scale) {
            this.flipx = flipx;
            this.scaleCloud = scale;
        }

        public void setRegion(TextureRegion region) {
            this.regCloud = region;
        }

        public void render(SpriteBatch batch) {
            TextureRegion reg = this.regCloud;
            SpriteBatch spriteBatch = batch;
            spriteBatch.draw(reg.getTexture(), this.origin.x + this.position.x, this.origin.y + this.position.y, this.origin.x, this.origin.y, this.dimension.x, this.dimension.y, this.scaleCloud * this.scale.x, this.scaleCloud * this.scale.y, this.rotation, reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(), reg.getRegionHeight(), this.flipx, false);
        }
    }

    public ObjectClouds(float length, float height) {
        this.length = length;
        this.position.y = height;
        init();
    }

    public void init() {
        this.dimension.set(3.0f, 1.5f);
        this.regCloud = Assets.instance.levelDecoration.cloud;
        int numClouds = (int) (this.length / ((float) 5));
        this.clouds = new Array(numClouds * 4);
        for (int i = 0; i < numClouds; i++) {
            Cloud cloud = spawnCloud();
            cloud.position.x = (float) (i * 5);
            this.clouds.add(cloud);
        }
    }

    private Cloud spawnCloud() {
        Cloud cloud = new Cloud(MathUtils.randomBoolean(), MathUtils.random(0.4f, 0.7f));
        cloud.dimension.set(this.dimension);
        cloud.setRegion(this.regCloud);
        Vector2 pos = new Vector2();
        pos.x = this.length + 10.0f;
        pos.y += this.position.y;
        pos.y = (((float) (MathUtils.randomBoolean() ? 1 : -1)) * MathUtils.random(0.0f, 1.0f)) + pos.y;
        cloud.position.set(pos);
        Vector2 speed = new Vector2();
        speed.x += 0.5f;
        speed.x += MathUtils.random(0.0f, 0.5f);
        cloud.terminalVelocity.set(speed);
        speed.x *= -1.0f;
        cloud.velocity.set(speed);
        return cloud;
    }

    public void render(SpriteBatch batch) {
        Iterator it = this.clouds.iterator();
        while (it.hasNext()) {
            ((Cloud) it.next()).render(batch);
        }
    }

    public void update(float deltaTime) {
        for (int i = this.clouds.size - 1; i >= 0; i--) {
            Cloud cloud = (Cloud) this.clouds.get(i);
            cloud.update(deltaTime);
            if (cloud.position.x < -10.0f) {
                this.clouds.removeIndex(i);
                this.clouds.add(spawnCloud());
            }
        }
    }
}
