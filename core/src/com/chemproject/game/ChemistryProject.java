package com.chemproject.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.chemproject.game.Screens.DirectedGame;
import com.chemproject.game.Screens.Level2Screen;
import com.chemproject.game.Screens.Level3Screen;
import com.chemproject.game.Screens.MenuScreen;
import com.chemproject.game.Screens.SesiBukuScreen;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.AudioManager;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.ConversationPanelBuilder;
import com.chemproject.game.Utils.GamePreferences;
import com.chemproject.game.Utils.URIHandler;

public class ChemistryProject extends DirectedGame {
    public static ConversationPanelBuilder builder;
    public URIHandler uriHandler;

    public ChemistryProject(URIHandler handler) {
        this.uriHandler = handler;
    }

    public void create() {
        Gdx.app.setLogLevel(3);
        Assets.instance.init(new AssetManager());
        GamePreferences.instance.load();
        AudioManager.instance.play(Assets.instance.music.song1);
        builder = new ConversationPanelBuilder();
        setScreen(new MenuScreen(this));
//        setScreen(new SesiBukuScreen(this, 1, new MenuScreen(this)));
//        setScreen(new Level3Screen(this), Constants.screenSlice);
//        setScreen(new Level2Screen(this));
//        setScreen(new ProfilScreen(this));
    }

    public void dispose() {
        super.dispose();
    }

    public void resize(int width, int height) {
        super.resize(width, height);
    }
}
