package com.chemproject.game;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Disposable;
import com.chemproject.game.Screens.Panel.AndroidController;
import com.chemproject.game.Screens.Panel.UIGame;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.WorldController.STATE;

public class WorldRenderer implements Disposable {
    private SpriteBatch batch;
    private OrthographicCamera cam;
    private OrthographicCamera cameraGUI;
    private UIGame uiGame;
    private WorldController worldController;

    public WorldRenderer(WorldController worldController) {
        this.worldController = worldController;
        init();
    }

    public void init() {
        this.batch = new SpriteBatch();
        this.cam = new OrthographicCamera(8.0f, 8.0f);
        this.cam.position.set(0.0f, 0.0f, 0.0f);
        this.cam.update();
        this.cameraGUI = new OrthographicCamera(Constants.VIEWPORT_GUI_WIDTH, Constants.VIEWPORT_GUI_HEIGHT);
        this.cameraGUI.position.set(0.0f, 0.0f, 0.0f);
        this.cameraGUI.setToOrtho(true);
        this.cameraGUI.update();
        this.uiGame = new UIGame(this.cameraGUI, this.batch);
        uiGame.init(1);
        worldController.setAndroidController(uiGame.getAndroidController());
        this.worldController.conver.setPanels("level1");
    }

    public void render() {
        renderWorld(this.batch);
        if (this.worldController.beginConversation) {
            this.worldController.conver.beginConversation(new Image(Assets.instance.assetBook.bgDialog));
        }
        if (this.worldController.conver.isConversationDone) {
            renderGUI();
            this.worldController.cameraHelper.setTarget(this.worldController.level.bunnyHead);
            this.worldController.beginConversation = false;
            return;
        }
        this.worldController.conver.render();
    }

    private void renderWorld(SpriteBatch batch) {
        this.worldController.cameraHelper.applyTo(this.cam);
        batch.setProjectionMatrix(this.cam.combined);
        batch.begin();
        this.worldController.level.render(batch);
        batch.end();
    }

    public void resize(int width, int height) {
        this.cam.viewportWidth = (8.0f / ((float) height)) * ((float) width);
        this.cam.update();
        this.cameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
        this.cameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT / ((float) height)) * ((float) width);
        this.cameraGUI.position.set(this.cameraGUI.viewportWidth / 2.0f, this.cameraGUI.viewportHeight / 2.0f, 0.0f);
        this.cameraGUI.update();
        this.uiGame.resize(width, height);
        this.worldController.conver.resize((float) width, (float) height);
    }

    private void renderGUI() {
        this.uiGame.updateScore(this.worldController.score);
        this.uiGame.updateLives(this.worldController.lives);
        this.uiGame.updateFpsCounter();
        if (this.worldController.state == STATE.WIN) {
            this.uiGame.gameWin(true);
        } else if (this.worldController.state == STATE.LOSE) {
            this.uiGame.gameWin(false);
        }
        this.uiGame.draw();
        if (uiGame.toMenu) {
            worldController.backToMenu();
        }
    }

//    private void renderGuiScore(SpriteBatch batch) {
//        float offsetX = 50.0f;
//        float offsetY = 50.0f;
//        if (this.worldController.scoreVisual < ((float) this.worldController.score)) {
//            long shakeAlpha = System.currentTimeMillis() % 360;
//            offsetX = 50.0f + (MathUtils.sinDeg(((float) shakeAlpha) * 2.2f) * 1.5f);
//            offsetY = 50.0f + (MathUtils.sinDeg(((float) shakeAlpha) * 2.9f) * 1.5f);
//        }
//        batch.draw(Assets.instance.goldCoin.coin, -15.0f, -15.0f, offsetX, offsetY, 100.0f, 100.0f, 0.35f, 0.35f, 0.0f);
//        Assets.instance.fonts.defaultBig.draw((Batch) batch, "" + ((int) this.worldController.scoreVisual), 75.0f - 0.28125f, 37.0f - 0.28125f);
//    }
//
//    private void renderGuiExtraLive(SpriteBatch batch) {
//        int i;
//        float x = (this.cameraGUI.viewportWidth - 50.0f) - 150.0f;
//        for (i = 0; i < 3; i++) {
//            if (this.worldController.lives <= i) {
//                batch.setColor(0.5f, 0.5f, 0.5f, 0.5f);
//            }
//            batch.draw(Assets.instance.bunny.heart, x + ((float) (i * 50)), -15.0f, 50.0f, 50.0f, 120.0f, 100.0f, 0.35f, -0.35f, 0.0f);
//            batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
//        }
//        if (this.worldController.lives >= 0 && this.worldController.livesVisual > ((float) this.worldController.lives)) {
//            i = this.worldController.lives;
//            float alphaColor = Math.max(0.0f, (this.worldController.livesVisual - ((float) this.worldController.lives)) - 0.5f);
//            float alphaScale = (0.35f * (((float) (this.worldController.lives + 2)) - this.worldController.livesVisual)) * 2.0f;
//            float alphaRotate = -45.0f * alphaColor;
//            batch.setColor(1.0f, 0.7f, 0.7f, alphaColor);
//            batch.draw(Assets.instance.bunny.heart, x + ((float) (i * 50)), -15.0f, 50.0f, 50.0f, 120.0f, 100.0f, alphaScale, -alphaScale, alphaRotate);
//            batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
//        }
//    }
//
//    private void renderGuiGameOverMessage(SpriteBatch batch) {
//        float x = this.cameraGUI.viewportWidth / 2.0f;
//        float y = this.cameraGUI.viewportHeight / 2.0f;
//        if (this.worldController.isGameOver()) {
//            BitmapFont fontGameOver = Assets.instance.fonts.defaultBig;
//            fontGameOver.setColor(1.0f, 0.0f, 0.0f, 1.0f);
//            fontGameOver.draw(batch, "GAME OVER", x, y, 0.0f, 1, true);
//            fontGameOver.setColor(1.0f, 1.0f, 1.0f, 1.0f);
//        }
//    }
//
//    private void renderGuiFeatherPowerUp(SpriteBatch batch) {
//        float timeLeftFeatherPowerUp = this.worldController.level.bunnyHead.timeLeftFeatherPowerUp;
//        if (timeLeftFeatherPowerUp > 0.0f) {
//            if (timeLeftFeatherPowerUp < 4.0f && ((int) (5.0f * timeLeftFeatherPowerUp)) % 2 != 0) {
//                batch.setColor(1.0f, 1.0f, 1.0f, 0.5f);
//            }
//            batch.draw(Assets.instance.feather.feather, -15.0f, 30.0f, 50.0f, 50.0f, 100.0f, 100.0f, 0.35f, -0.35f, 0.0f);
//            batch.setColor(1.0f, 1.0f, 1.0f, 1.0f);
//            Assets.instance.fonts.defaultSmall.draw((Batch) batch, "" + ((int) timeLeftFeatherPowerUp), 60.0f - 0.28125f, 57.0f + 30.0f);
//        }
//    }
//
//    private void renderGuiFpsCounter(SpriteBatch batch) {
//        float x = this.cameraGUI.viewportWidth / 2.0f;
//        int fps = Gdx.graphics.getFramesPerSecond();
//        BitmapFont fpsFont = Assets.instance.fonts.defaultNormal;
//        if (fps >= 45) {
//            fpsFont.setColor(0.0f, 1.0f, 0.0f, 1.0f);
//        } else if (fps >= 30) {
//            fpsFont.setColor(1.0f, 1.0f, 0.0f, 1.0f);
//        } else {
//            fpsFont.setColor(1.0f, 0.0f, 0.0f, 1.0f);
//        }
//        fpsFont.draw((Batch) batch, "FPS: " + fps, x, 15.0f);
//        fpsFont.setColor(1.0f, 1.0f, 1.0f, 1.0f);
//    }

    public void dispose() {
        this.batch.dispose();
        this.uiGame.dispose();
        this.worldController.conver.dispose();
    }

    public SpriteBatch getBatch() {
        return this.batch;
    }

    public UIGame getUiGame() {
        return uiGame;
    }
}
