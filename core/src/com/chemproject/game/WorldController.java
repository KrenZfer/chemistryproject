package com.chemproject.game;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Rectangle;
import com.chemproject.game.ObjectGame.BunnyHead;
import com.chemproject.game.ObjectGame.BunnyHead.JUMP_STATE;
import com.chemproject.game.ObjectGame.ObjectFeather;
import com.chemproject.game.ObjectGame.ObjectLarutan;
import com.chemproject.game.ObjectGame.ObjectLarutan.Type;
import com.chemproject.game.ObjectGame.ObjectRock;
import com.chemproject.game.Screens.DirectedGame;
import com.chemproject.game.Screens.GameScreen;
import com.chemproject.game.Screens.Level2Screen;
import com.chemproject.game.Screens.MenuScreen;
import com.chemproject.game.Screens.Panel.AndroidController;
import com.chemproject.game.Screens.SesiBukuScreen;
import com.chemproject.game.Utils.Assets;
import com.chemproject.game.Utils.AudioManager;
import com.chemproject.game.Utils.CameraHelper;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.ConversationPanelExec;
import java.util.Iterator;

public class WorldController extends InputAdapter {
    private static final String TAG = ChemistryProject.class.getName();
    public boolean beginConversation;
    public CameraHelper cameraHelper;
    public ConversationPanelExec conver;
    private DirectedGame game;
    public Level level;
    public int lives;
    public float livesVisual;
    private boolean lostLive;
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();
    public int score;
    public float scoreVisual;
    public STATE state;
    private AndroidController androidController;
    private float timeLeftGameOverDelay;
    private float timeforDelay;

    public enum STATE {
        WIN,
        LOSE,
        PLAY
    }

    public WorldController(DirectedGame game) {
        this.game = game;
        init();
    }

    public void initLevel() {
        this.score = 0;
        this.scoreVisual = (float) this.score;
        this.state = STATE.PLAY;
        this.lostLive = false;
        this.level = new Level(Constants.LEVEL_CHEM);
        this.cameraHelper.setPosition(this.level.end.x, this.level.end.y);
        this.timeLeftGameOverDelay = 3.0f;
        this.beginConversation = false;
        this.timeforDelay = 3.0f;
        this.conver = new ConversationPanelExec();
        this.conver.init();
    }

    public void init() {
        this.cameraHelper = new CameraHelper();
        this.lives = 3;
        this.livesVisual = (float) this.lives;
        initLevel();
    }

    public void backToMenu() {
        this.game.setScreen(new MenuScreen(this.game));
    }

    private void continueToSesiBuku() {
        this.game.setScreen(new SesiBukuScreen(this.game, 1, new Level2Screen(this.game)), Constants.screenSlide);
    }

    public void update(float deltaTime) {
        this.timeforDelay -= deltaTime;
        if (this.timeforDelay < 0.0f) {
            this.beginConversation = true;
        }
        if (this.lives <= 0) {
            this.state = STATE.LOSE;
        }
        if (isGameOver()) {
            this.timeLeftGameOverDelay -= deltaTime;
            if (this.timeLeftGameOverDelay < 0.0f) {
                if (this.state == STATE.LOSE) {
                    this.game.setScreen(new GameScreen(this.game), Constants.screenSlide);
                } else if (this.state == STATE.WIN) {
                    continueToSesiBuku();
                }
                this.timeLeftGameOverDelay = 3.0f;
            }
        } else {
            handleInputGame(deltaTime);
            handledebugInput(deltaTime);
        }
        this.level.update(deltaTime);
        testCollisions();
        this.cameraHelper.update(deltaTime);
        if (!isGameOver() && (isPlayerInWater() || this.lostLive)) {
            AudioManager.instance.play(Assets.instance.sounds.liveLost);
            this.lives--;
            if (isPlayerInWater()) {
                initLevel();
            }
            this.lostLive = false;
        }
        if (playerGotAllSolution()) {
            this.level.endPoint.activate();
        }
        this.level.mountains.updateScrollPosition(this.cameraHelper.getPosition());
        if (this.livesVisual > ((float) this.lives)) {
            this.livesVisual = Math.max((float) this.lives, this.livesVisual - (1.0f * deltaTime));
        }
        if (this.scoreVisual < ((float) this.score)) {
            this.scoreVisual = Math.min((float) this.score, this.scoreVisual + (250.0f * deltaTime));
        }
    }

    private boolean playerGotAllSolution() {
        return this.score == Assets.instance.assetFormulaContent.larutanPenyangga.size;
    }

    private void handledebugInput(float deltaTime) {
        if (Gdx.app.getType() == ApplicationType.Desktop && !this.cameraHelper.hasTarget(this.level.bunnyHead)) {
            float camMoveSpeed = 5.0f * deltaTime;
            if (Gdx.input.isKeyPressed(59)) {
                camMoveSpeed *= 5.0f;
            }
            if (Gdx.input.isKeyPressed(21)) {
                moveCamera(-camMoveSpeed, 0.0f);
            }
            if (Gdx.input.isKeyPressed(22)) {
                moveCamera(camMoveSpeed, 0.0f);
            }
            if (Gdx.input.isKeyPressed(19)) {
                moveCamera(0.0f, camMoveSpeed);
            }
            if (Gdx.input.isKeyPressed(20)) {
                moveCamera(0.0f, -camMoveSpeed);
            }
            if (Gdx.input.isKeyPressed(62)) {
                this.cameraHelper.setTarget(this.level.bunnyHead);
            }
        }
    }

    public void handleInputGame(float deltaTime) {
        if (this.cameraHelper.hasTarget(this.level.bunnyHead)) {
            if (Gdx.input.isKeyPressed(29) || androidController.isLeftKeyPressed()) {
                bunnyMaju(false);
            } else if (Gdx.input.isKeyPressed(32) || androidController.isRightKeyPressed()) {
                bunnyMaju(true);
            }
            if (androidController.isJumpKeyPressed() || Gdx.input.isKeyPressed(51)) {
                bunnyLompat(true);
            } else {
                bunnyLompat(false);
            }
            if (Gdx.input.isKeyPressed(62)) {
                this.cameraHelper.setTarget(null);
            }
        }
    }

    public void bunnyMaju(boolean maju){
        if (maju) {
            this.level.bunnyHead.velocity.x = this.level.bunnyHead.terminalVelocity.x;
        }else {
            this.level.bunnyHead.velocity.x = -this.level.bunnyHead.terminalVelocity.x;
        }
    }

    public void bunnyLompat(boolean lompat){
        this.level.bunnyHead.setJumping(lompat);
    }

    private void moveCamera(float x, float y) {
        this.cameraHelper.setPosition(x + this.cameraHelper.getPosition().x, y + this.cameraHelper.getPosition().y);
    }

    public boolean keyUp(int keycode) {
        if (keycode == 46) {
            init();
            Gdx.app.debug(TAG, "Game World Reseted");
        } else if (keycode == 66) {
            this.cameraHelper.setTarget(this.cameraHelper.hasTarget() ? null : this.level.bunnyHead);
            Gdx.app.debug(TAG, "Camera follow enabled : " + this.cameraHelper.hasTarget());
        } else if (keycode == Keys.ESCAPE) {
            backToMenu();
        }
        return false;
    }

    private void onCollisionBunnyWithRock(ObjectRock rock) {
        boolean hitBottomEdge;
        BunnyHead bunnyHead = this.level.bunnyHead;
        if (bunnyHead.position.y + bunnyHead.bounds.height >= rock.position.y + rock.bounds.height || bunnyHead.position.y + bunnyHead.bounds.height <= rock.position.y) {
            hitBottomEdge = false;
        } else {
            hitBottomEdge = true;
        }
        float heightDifference = Math.abs((bunnyHead.position.y + bunnyHead.bounds.height) - rock.position.y);
        if (hitBottomEdge && heightDifference < 0.5f) {
            bunnyHead.position.y = rock.position.y - bunnyHead.bounds.height;
            bunnyHead.jumpState = JUMP_STATE.FALLING;
        } else if (Math.abs(bunnyHead.position.y - (rock.position.y + rock.bounds.height)) > 0.5f) {
            boolean hitRightEdge;
            if (bunnyHead.position.x > rock.position.x + (rock.bounds.width / 2.0f)) {
                hitRightEdge = true;
            } else {
                hitRightEdge = false;
            }
            if (hitRightEdge) {
                bunnyHead.position.x = rock.position.x + rock.bounds.width;
                return;
            }
            bunnyHead.position.x = rock.position.x - bunnyHead.bounds.width;
        } else {
            switch (bunnyHead.jumpState) {
                case GROUNDED:
                    return;
                case FALLING:
                case JUMP_FALLING:
                    bunnyHead.position.y = (rock.position.y + bunnyHead.bounds.height) + bunnyHead.origin.y;
                    bunnyHead.jumpState = JUMP_STATE.GROUNDED;
                    return;
                case JUMP_RISING:
                    bunnyHead.position.y = (rock.position.y + bunnyHead.bounds.height) + bunnyHead.origin.y;
                    return;
                default:
                    return;
            }
        }
    }

    private void onCollisionBunnyWithLarutan(ObjectLarutan larutan) {
        boolean z = true;
        larutan.isCollected = true;
        if (isplayerTookRightSolution(larutan)) {
            z = false;
        }
        this.lostLive = z;
        if (!this.lostLive) {
            Assets.instance.sounds.pickupCoin.play();
            this.score++;
        }
    }

    private void onCollisionBunnyWithFeather(ObjectFeather feather) {
        feather.collected = true;
        this.score += feather.getScore();
        this.level.bunnyHead.setFeatherPowerUp(true);
        Gdx.app.log(TAG, "Feather collected");
    }

    private void onCollisionWithEndPortal() {
        if (this.level.endPoint.isActive) {
            this.state = STATE.WIN;
        }
    }

    private void testCollisions() {
        this.r1.set(this.level.bunnyHead.position.x, this.level.bunnyHead.position.y, this.level.bunnyHead.bounds.width, this.level.bunnyHead.bounds.height);
        this.r2.set(this.level.endPoint.position.x, this.level.endPoint.position.y, this.level.endPoint.dimension.x, this.level.endPoint.dimension.y);
        if (this.r1.overlaps(this.r2)) {
            onCollisionWithEndPortal();
        }
        Iterator it = this.level.rocks.iterator();
        while (it.hasNext()) {
            ObjectRock rock = (ObjectRock) it.next();
            this.r2.set(rock.position.x, rock.position.y, rock.bounds.width, rock.bounds.height);
            if (this.r1.overlaps(this.r2)) {
                onCollisionBunnyWithRock(rock);
            }
        }
        it = this.level.larutan.iterator();
        while (it.hasNext()) {
            ObjectLarutan larutan = (ObjectLarutan) it.next();
            if (!larutan.isCollected) {
                this.r2.set(larutan.position.x, larutan.position.y, larutan.bounds.width, larutan.bounds.height);
                if (this.r1.overlaps(this.r2)) {
                    onCollisionBunnyWithLarutan(larutan);
                    return;
                }
            }
        }
    }

    public boolean isGameOver() {
        return this.state == STATE.WIN || this.state == STATE.LOSE;
    }

    public boolean isPlayerInWater() {
        return this.level.bunnyHead.position.y < -5.0f;
    }

    public boolean isplayerTookRightSolution(ObjectLarutan larutan) {
        if (larutan.type == Type.PENYANGGA) {
            return true;
        }
        return false;
    }

    public void setAndroidController(AndroidController androidController) {
        this.androidController = androidController;
    }
}
