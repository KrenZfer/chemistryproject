package com.chemproject.game.desktop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.chemproject.game.ChemistryProject;
import com.chemproject.game.Utils.Constants;
import com.chemproject.game.Utils.URIHandler;

public class DesktopLauncher
		implements URIHandler
{
	private static boolean rebuildAtlas = false;
	private static boolean drawDebugOutLine = false;
	public static DesktopLauncher desktopLauncher;

	public static void main(String[] arg)
	{
		if (desktopLauncher == null) {
			desktopLauncher = new DesktopLauncher();
		}
		if (rebuildAtlas)
		{
			TexturePacker.Settings settings = new TexturePacker.Settings();
//			settings.maxWidth = 4096;
//			settings.maxHeight = 4096;
			settings.duplicatePadding = false;
			settings.filterMin = Texture.TextureFilter.Linear;
			settings.filterMag = Texture.TextureFilter.Linear;
			settings.debug = drawDebugOutLine;
//			TexturePacker.process(settings, "../../desktop/assets-raw/images/", "images", "bunny_pack.atlas");
//			settings.maxWidth = 2048;
//			settings.maxHeight = 2048;
//			TexturePacker.process(settings, "../../desktop/assets-raw/images-ui/", "images-ui", "bunny_ui.atlas");
			settings.maxHeight = 16384;
			settings.maxWidth = 16384;
			TexturePacker.process(settings, "../../desktop/assets-raw/menuingame/", "images-ui", "menuingame.atlas");
//			TexturePacker.process(settings, "../../desktop/assets-raw/images_choosebook/", "images", "choose_book.atlas");
//
//			TexturePacker.process(settings, "../../desktop/assets-raw/kontenbuku/", "images", "konten_buku");
//
//			TexturePacker.process(settings, "../../desktop/assets-raw/larutan/", "images", "larutan");
//
//			TexturePacker.process(settings, "../../desktop/assets-raw/Karakter/", "images", "karakter");
//
//			TexturePacker.process(settings, "../../desktop/assets-raw/soalLevel3/", "images", "soallevel3");
//
//			TexturePacker.process(settings, "../../desktop/assets-raw/soallatihan/", "images", "soalLatihan");
		}
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "ChemistryProject";
		config.width = Constants.BIG_SCREEN_WIDTH;
		config.height = Constants.BIG_SCREEN_HEIGHT;

		new LwjglApplication(new ChemistryProject(desktopLauncher), config);
	}

	public void openURI(String uri)
	{
		Gdx.net.openURI(uri);
	}
}
