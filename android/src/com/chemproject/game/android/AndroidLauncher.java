package com.chemproject.game.android;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.surfaceview.RatioResolutionStrategy;
import com.chemproject.game.ChemistryProject;
import com.chemproject.game.Utils.URIHandler;

public class AndroidLauncher extends AndroidApplication implements URIHandler {
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.resolutionStrategy = new RatioResolutionStrategy(1280.0f, 720.0f);
        config.useImmersiveMode = true;
        initialize(new ChemistryProject(this), config);
    }

    public void openURI(String uri) {
        startActivity(new Intent("android.intent.action.VIEW", Uri.parse(uri)));
    }
}
